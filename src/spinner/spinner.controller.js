(function () {
    'use strict';
    
    var Spinner = angular.module('Spinner');

    /***************************************************************************
     ***** SPINNER CONTROLLER
     ***************************************************************************/
    Spinner.controller('SpinnerController', SpinnerController);
    SpinnerController.$inject = ['$rootScope', '$scope'];
    function SpinnerController($rootScope, $scope) {
        alert("spinner");
        var spinner = this;
        spinner.showSpinner = false;
        //returns a cancellation function
        var deregister = $rootScope.$on('loading', function (event, data) {
            console.log(event);
            spinner.showSpinner = data.on;
        });
        $scope.$on('$destroy', deregister);
    }
    ;

})();

