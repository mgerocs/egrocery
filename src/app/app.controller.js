(function () {
    'use strict';

    var GroceryApp = angular.module('GroceryApp');

    /***************************************************************************
     ROOT CONTROLLER
     ***************************************************************************/
    GroceryApp.controller('RootController', RootController);
    RootController.$inject = ['$scope', '$location', 'User', 'Cart', 'ProductCategory', 'Region', 'Navigation', 'BaseUrl'];
    function RootController($scope, $location, User, Cart, ProductCategory, Region, Navigation, BaseUrl) {
        var root = this;
        root.base = BaseUrl;
        root.currentMenuItem = Navigation.getCurrentMenuItem();

        root.isLoggedIn = User.checkIfLoggedIn();
        root.user = User.getEmail();

        root.categories = ProductCategory.getCategories();
        root.regions = Region.getRegions();

        root.cart = Cart.getCart();

        $scope.$watch(
                function () {
                    return User.getUser();
                },
                function (newValue, oldValue) {
                    root.isLoggedIn = newValue.is_logged_in;
                    root.user = newValue.email;
                }, true
                );

        $scope.$watch(
                function () {
                    return Cart.getCart();
                },
                function (newValue, oldValue) {
                    root.cart = newValue;
                }, true
                );

        $scope.$watch(
                function () {
                    return Navigation.getCurrentMenuItem();
                },
                function (newValue, oldValue) {
                    root.currentMenuItem = newValue;
                }, true
                );

        root.go = function (path) {
            $location.path(path);
        };

        root.focussed = false;

        root.pageClass = "default-page";


//        root.getClass = function (path) {
//            return ($location.path().substr(0, path.length) === path) ? 'active' : '';
//        };
    }
    ;

    /***************************************************************************
     ***** IDENTITY CONTROLLER
     ***************************************************************************/
    GroceryApp.controller('IdentityController', IdentityController);
    IdentityController.$inject = ['$rootScope', '$scope', 'User'];
    function IdentityController($rootScope, $scope, User) {
        var identity = this;
        identity.error = "";
        identity.email = "";
        identity.password = "";
        $scope.focussed = false;

        identity.dropDown = function () {
            $scope.focussed = true;
        };

        identity.login = function () {
            $rootScope.$broadcast("loading", {on: true});
            var promise = User.login(identity.email, identity.password);
            promise.
                    then(function (response) {
                        identity.error = response.data.error;
                        identity.email = "";
                        identity.password = "";
                        $scope.loginform.$setPristine();
                        $scope.loginform.$setValidity();
                        User.setIfLoggedIn(response.data.is_logged_in);
                        User.setEmail(response.data.email);
                    })
                    .catch(function (error) {
                        console.log("Error:", error);
                    })
                    .finally(function () {
                        $rootScope.$broadcast("loading", {on: false});
                    });

        };

        identity.logout = function () {
            $rootScope.$broadcast("loading", {on: true});
            var promise = User.logout();
            promise.
                    then(function (response) {
                        identity.error = "";
                        identity.email = "";
                        identity.password = "";
                        $scope.loginform.$setPristine();
                        $scope.loginform.$setValidity();
                        User.setIfLoggedIn(false);
                        User.setEmail("");
                    })
                    .catch(function (error) {
                        console.log("Error:", error);
                    })
                    .finally(function () {
                        $rootScope.$broadcast("loading", {on: false});
                    });
        };

        identity.innerClick = function ($event) {
            var id = $event.target.id;
            if (id === "paybutton" || id === "regbutton") {
                return true;
            }
            $event.stopPropagation();
            return false;
        };
    }

    /***************************************************************************
     ***** HOME CONTROLLER
     ***************************************************************************/
    GroceryApp.controller('HomeController', HomeController);
    HomeController.$inject = ['$location', 'ProductFilter', 'Navigation'];
    function HomeController($location, ProductFilter, Navigation) {
        var home = this;
        Navigation.setCurrentMenuItem("");
        home.selectCategory = function (category_id) {
            ProductFilter.reset();
            ProductFilter.setFilterCategory(category_id);
            $location.path("/products");
        };
    }

    /***************************************************************************
     ***** NAV CONTROLLER
     ***************************************************************************/
    GroceryApp.controller('NavController', NavController);
    NavController.$inject = ['Navigation'];
    function NavController(Navigation) {
        var nav = this;
        nav.setCurrentMenuItem = function (menuitem) {
            Navigation.setCurrentMenuItem(menuitem);
        };
    }

    /***************************************************************************
     ***** PRODUCT CONTROLLER
     ***************************************************************************/
    GroceryApp.controller('ProductController', ProductController);
    ProductController.$inject = ['$scope', '$rootScope', '$route', '$routeParams', '$q', '$location', '$sessionStorage', 'ProductCategory', 'Product', 'ProductFilter', 'Farm', 'Cart', 'Navigation'];
    function ProductController($scope, $rootScope, $route, $routeParams, $q, $location, $sessionStorage, ProductCategory, Product, ProductFilter, Farm, Cart, Navigation) {

        var product = this;
        Navigation.setCurrentMenuItem("products");

        //if categories are not set redirect home
        if (!$sessionStorage.categories) {
            $location.path("/home");
        } else {
            ProductCategory.setCategories($sessionStorage.categories);
        }
        //default to "zöldség"
        if (!ProductFilter.getFilter().category_id) {
            ProductFilter.setFilterCategory(1);
        }

        product.title = 'Termékek';
        //$rootScope.currentNavItem = 'products';

        product.category_id = ProductFilter.getFilter().category_id;
        product.category_name = ProductCategory.getCategoryName(product.category_id);

        if (!$routeParams.id) {
            ProductFilter.setFilterPId(null);
        } else {
            ProductFilter.setFilterCategory(null);
            ProductFilter.setFilterPId($routeParams.id);
        }

        $rootScope.$broadcast("loading", {on: true});

        var promise0 = Product.getProductLabels(product.category_id);
        var promise1 = Product.getProducts(ProductFilter.getFilter());
        var promise2 = Farm.getFarms();

        $q.all([promise0, promise1, promise2])
                .then(function (response) {
                    product.labels = (response[0].data.items || []);
                    product.products = (response[1].data.items || []);
                    product.farms = (response[2].data.items || []);
                })
                .catch(function (error) {
                    console.log("Error:", error);
                })
                .finally(function () {
                    $rootScope.$broadcast("loading", {on: false});
                });

        //initialize product filters
        product.labelFilter = (ProductFilter.getFilter().label || "");
        product.farmFilter = (ProductFilter.getFilter().farm_id || "");
        product.regionFilter = (ProductFilter.getFilter().region_id || "");

        //category filter
        product.selectCategory = function (category_id) {
            ProductFilter.reset();
            ProductFilter.setFilterCategory(category_id);
            product.category_id = ProductFilter.getFilter().category_id;
            product.category_name = ProductCategory.getCategoryName(product.category_id);
            $route.reload();
        };
        //update product filters
        product.setLabelFilter = function (label) {
            ProductFilter.setFilterLabel(label);
            product.labelFilter = label;
        };
        product.setRegionFilter = function () {
            ProductFilter.setFilterRegion(product.regionFilter);
        };
        product.setFarmFilter = function () {
            ProductFilter.setFilterFarm(product.farmFilter);
        };

//        product.setProductFilter = function (category_id, region_id, farm_id, label) {
//            ProductFilter.setFilterCategory(category_id);
//            ProductFilter.setFilterRegion(region_id);
//            ProductFilter.setFilterFarm(farm_id);
//            ProductFilter.setFilterLabel(label);
//            $location.path("/products");
//        };

        //add to cart        
        product.addToCart = function (item) {
            if (item && item.amount !== 0) {
                Cart.addToCart(item);
            }
            $scope.count = 1;
            //cart button animation
            var $cartButton = angular.element(document.querySelector('#cart-button'));
            $cartButton.addClass('added_item');
            setTimeout(function () {
                $cartButton.removeClass('added_item');
            }, 500);
        };
        //check if in cart
        product.checkIfInCart = function (product) {
            return Cart.checkIfInCart(product);
        };
    }

    /***************************************************************************
     ***** FARM CONTROLLER
     ***************************************************************************/
    GroceryApp.controller('FarmController', FarmController);
    FarmController.$inject = ['$rootScope', '$routeParams', '$location', '$timeout', '$sessionStorage', 'Farm', 'ProductCategory', 'ProductFilter', 'Navigation'];
    function FarmController($rootScope, $routeParams, $location, $timeout, $sessionStorage, Farm, ProductCategory, ProductFilter, Navigation) {
        var farm = this;
        farm.lat = 0;
        farm.lon = 0;
        Navigation.setCurrentMenuItem("farms");

        farm.showMap = true;

        if (!$sessionStorage.categories) {
            $location.path("/home");
        } else {
            ProductCategory.setCategories($sessionStorage.categories);
        }

        farm.title = 'Gazdaságok';

        $rootScope.$broadcast("loading", {on: true});

        var promise = {};
        if ($routeParams.id) {
            promise = Farm.getFarm($routeParams.id);
        } else {
            promise = Farm.getFarms();
        }

        promise
                .then(function (response) {
                    farm.farms = (response.data.items || []);
                    $timeout(function () {
                        farm.lat = farm.farms[0].lat;
                        farm.lon = farm.farms[0].lon;
                        var $map = document.getElementById('map');
                        if ($map) {
                            Farm.showOnMap(farm.farms[0].lat, farm.farms[0].lon);
                        }
                    });
                })
                .catch(function (error) {
                    console.log("Error:", error);
                })
                .finally(function () {
                    $rootScope.$broadcast("loading", {on: false});
                });

        farm.Math = window.Math;
        farm.sortVal = 'name';
        farm.sortDir = false;

        farm.change = function () {
            if (farm.sortVal === "name") {
                farm.sortVal = "rating";
                farm.sortDir = true;
            } else {
                farm.sortVal = "name";
                farm.sortDir = false;
            }
        };

        farm.setProductFilter = function (category_id, region_id, farm_id, label) {
            ProductFilter.setFilterCategory(category_id);
            ProductFilter.setFilterRegion(region_id);
            ProductFilter.setFilterFarm(farm_id);
            ProductFilter.setFilterLabel(label);
            $location.path("/products");
        };

        farm.togglePanel = function () {
            farm.showMap = !farm.showMap;
        }
        ;

    }

    /***************************************************************************
     ***** CART CONTROLLER
     ***************************************************************************/
    GroceryApp.controller('CartController', CartController);
    CartController.$inject = ['$scope', 'Cart', 'Navigation'];
    function CartController($scope, Cart, Navigation) {
        var cart = this;
        cart.title = "Kosár";

        Navigation.setCurrentMenuItem("");
        cart.modifyCart = function (id, amount) {
            Cart.modifyCart(id, amount);
        };
        cart.removeFromCart = function (id) {
            Cart.removeFromCart(id);
        };
//        cart.setProductFilter = function (category_id, region_id, farm_id, label) {
//            ProductFilter.setFilterCategory(category_id);
//            ProductFilter.setFilterRegion(region_id);
//            ProductFilter.setFilterFarm(farm_id);
//            ProductFilter.setFilterLabel(label);
//            $location.path("/products");
//        };

    }

    /***************************************************************************
     ***** DELIVERY CONTROLLER
     ***************************************************************************/
    GroceryApp.controller('DeliveryController', DeliveryController);
    DeliveryController.$inject = ['Navigation'];
    function DeliveryController(Navigation) {
        var delivery = this;
        delivery.title = "Szállítás";

        Navigation.setCurrentMenuItem("");
    }

    /***************************************************************************
     ***** SPINNER CONTROLLER
     ***************************************************************************/
    GroceryApp.controller('SpinnerController', SpinnerController);
    SpinnerController.$inject = ['$rootScope', '$scope'];
    function SpinnerController($rootScope, $scope) {
        var spinner = this;
        spinner.showSpinner = false;
        //returns a cancellation function
        var deregister = $rootScope.$on('loading', function (event, data) {
            console.log(event);
            spinner.showSpinner = data.on;
        });
        $scope.$on('$destroy', deregister);
    }
    ;

})();

