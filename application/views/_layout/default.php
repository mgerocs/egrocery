<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>e-grocery</title>
        <meta name="keywords" content="">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="icon" href="">

        <!-- CSS -->
       <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/style.css" />        

    </head>
    <body> 
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <!--<div ng-controller="TestController as test"></div>-->
        <div id="site-wrapper"  ng-controller="RootController as root" off-canvas-menu ng-class="{'show-nav':isMenuOpen}" class="container-fluid">                       

            <div id="site-canvas">

                <header>                        
                    <div class="row top-bar bg-lgreen">
                        <nav id="off-canvas-menu" class="navbar navbar-default">
                            <div class="container-fluid">                                
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggler" ng-click="toggleMenu()" style="float:left">                                        
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="sr-only">Toggle navigation</span>
                                    </button>                            
                                </div>
                                <div id="site-menu">                                   
                                    <menu-items></menu-items>                                    
                                </div>
                            </div>
                        </nav>
                        <div class="col-xs-12 col-sm-12 col-md-8 offset-md-2 no-padding">
                            <a href="#/home">
                                <img src="css/decor/logo.png" alt="ennivalo logo" class="logo img-fluid"/>
                            </a>      
                            <div class="header_controls">                               
                                <user-panel></user-panel>                                
                                <cart-panel></cart-panel>
                            </div>  
                        </div>                    
                    </div>
                    <div class="row bg-lgreen">
                        <div class="col-xs-12 col-sm-12 col-md-8 offset-md-2 no-padding">
                            <nav id="main-menu">                                
                                <menu-items></menu-items>
                            </nav>
                        </div>
                    </div>
                    <!--<div class="decor header-decor"></div>-->
                </header>

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-8 offset-md-2">
                        <div class="page {{ pageClass}}" ng-view autoscroll="true">
                            <!-- placeholder for views -->
                        </div>
                    </div>               
                </div>

                <footer>
                    <!--<div class="decor footer-decor"></div>-->
                    <div class="row bottom-bar bg-lgreen">
                        <div class="col-xs-12 col-sm-12 col-md-8 offset-md-2 no-padding">
                            <div class="col-xs-12 col-sm-6">
                                <img src="css/decor/logo.png" alt="ennivalo logo" class="img-fluid"/>
                            </div>                           
                            <div class="col-xs-12 col-sm-6">
                                <ul id="footer_menu" class="nav navbar-nav float-sm-right">
                                    <li>
                                        <a href="">Általános Szerződési Feltételek</a>
                                    </li>
                                    <li>
                                        <a href="">Kapcsolat</a>
                                    </li>
                                    <li>
                                        <a href="">GYIK</a>
                                    </li>
                                </ul>
                            </div>                          
                        </div>
                    </div>

                </footer>

            </div>             

        </div>

    <spinner></spinner>

    <!-- Libraries -->
    <!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/js/bootstrap.min.js" integrity="sha384-BLiI7JTZm+JWlgKa0M0kGRpJbF2J8q+qreVrKBC47e3K6BW78kGLrCkeRX6I9RoK" crossorigin="anonymous"></script>-->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script>   
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular-route.min.js"></script>   
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ngStorage/0.3.6/ngStorage.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.13/angular-animate.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/2.2.0/ui-bootstrap.min.js"></script>         
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/2.2.0/ui-bootstrap-tpls.min.js"></script>         
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBRAOuTAIm_ApThROJYHxh4avbhtTyVJuM" type="text/javascript"></script>      

    <script>
                            var base_url = '<?php echo base_url(); ?>';
    </script>             

    <!--Modules--> 
    <!--<script type="text/javascript" src="src/spinner/spinner.module.js"></script>-->
    <script type="text/javascript" src="src/app/app.module.js"></script>

    <!--bootstrap app--> 
    <script type="text/javascript" src="src/bootstrap.js"></script>               

    <!--'App' module artifacts--> 
    <script type="text/javascript" src="src/app/app.config.js"></script>
    <script type="text/javascript" src="src/app/app.controller.js"></script>
    <script type="text/javascript" src="src/app/app.service.js"></script>
    <script type="text/javascript" src="src/app/app.directive.js"></script>
    <script type="text/javascript" src="src/app/app.filter.js"></script>

    <!--'Spinner' module artifacts--> 
<!--    <script type="text/javascript" src="src/spinner/spinner.controller.js"></script>
    <script type="text/javascript" src="src/spinner/spinner.directive.js"></script>  -->

<!--<script type="text/javascript" src="js/script.js"></script>-->

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
                            //            (function (b, o, i, l, e, r) {
                            //                b.GoogleAnalyticsObject = l;
                            //                b[l] || (b[l] =
                            //                        function () {
                            //                            (b[l].q = b[l].q || []).push(arguments)
                            //                        });
                            //                b[l].l = +new Date;
                            //                e = o.createElement(i);
                            //                r = o.getElementsByTagName(i)[0];
                            //                e.src = 'https://www.google-analytics.com/analytics.js';
                            //                r.parentNode.insertBefore(e, r)
                            //            }(window, document, 'script', 'ga'));
                            //            ga('create', 'UA-XXXXX-X', 'auto');
                            //            ga('send', 'pageview');
    </script>
</body>
</html>
