<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class GroceryController extends Common {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $this->load->view('_layout/default');
    }

    function getProductCategories() {
        try {
            
//            $repo = $this->em->getRepository('Entity\ProductCategory');
//            $qb = $repo->createQueryBuilder('pc');

            $qb = $this->em->createQueryBuilder();
            $qb->select('pc.id', 'pc.name', 'pc.weight');
            $qb->from('Entity\ProductCategory', 'pc', 'pc.id');        
            $qb->orderBy('pc.weight', 'ASC');

            $categories = $qb->getQuery()->getArrayResult();

            echo json_encode(array('status' => true, 'items' => $categories));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function getProductLabels($category_id) {
        try {
            if (empty($category_id)) {
                throw new Exception("No category provided.");
            }
            $repo = $this->em->getRepository('Entity\Product');
            $qb = $repo->createQueryBuilder('p');

            $qb->select('p.label');
            $qb->distinct();
            $qb->where('IDENTITY (p.category) = :category_id');
            $qb->setParameter('category_id', $category_id);
            $qb->orderBy('p.label', 'ASC');

            $labels = $qb->getQuery()->getArrayResult();

            echo json_encode(array('status' => true, 'items' => $labels));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function getProducts() {
        try {

            $params = json_decode(file_get_contents('php://input'), true);

            $filter = isset($params['filter']) ? $params['filter'] : NULL;

            $category_id = NULL;
            $farm_id = NULL;
            $label = NULL;
            $product_id = NULL;

            if (isset($filter)) {
                $category_id = $filter['category_id'] ? $filter['category_id'] : NULL;
//                $farm_id = isset($filter['farm_id']) ? $filter['farm_id'] : NULL;
//                $label = isset($filter['label']) ? $filter['label'] : NULL;
                $product_id = isset($filter['product_id']) ? $filter['product_id'] : NULL;
            }

            $repo = $this->em->getRepository('Entity\Product');
            $qb = $repo->createQueryBuilder('p');

            $qb->select('p.id', 'IDENTITY (p.category) AS category_id', 'IDENTITY (p.farm) AS farm_id', 'IDENTITY (p.gallery) AS gallery_id', 'IDENTITY (f.region) AS region_id', 'p.status', 'p.price', 'p.measure', 'p.label', 'p.name', 'p.create_date', 'pc.name AS categoryname', 'f.name AS farmname', 'i.filename AS cover');
            $qb->leftJoin('p.category', 'pc');
            $qb->leftJoin('p.farm', 'f');
            $qb->leftJoin('p.gallery', 'g');
            $qb->leftJoin('g.images', 'i', 'WITH', 'i.cover = 1');

            if (!empty($category_id) && $category_id !== 0) {
                $qb->where('IDENTITY (p.category) = :category_id');
                $qb->setParameter('category_id', $category_id);
            }
//            if (!empty($label)) {
//                $qb->andWhere('p.label= :label');
//                $qb->setParameter('label', $label);
//            }
//            if (!empty($farm_id)) {
//                $qb->andWhere('IDENTITY (p.farm) = :farm_id');
//                $qb->setParameter('farm_id', $farm_id);
//            }
            if (!empty($product_id)) {
                $qb->andWhere('p.id = :product_id');
                $qb->setParameter('product_id', $product_id);
            }

            $qb->orderBy('p.name', 'ASC');

            $products = $qb->getQuery()->getArrayResult();

            echo json_encode(array('status' => true, 'items' => $products));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function getRegions() {
        try {
            $repo = $this->em->getRepository('Entity\Region');
            $qb = $repo->createQueryBuilder('r');

            $qb->select('r.id', 'r.name');

            $qb->orderBy('r.name', 'ASC');

            $regions = $qb->getQuery()->getArrayResult();

            echo json_encode(array('status' => true, 'items' => $regions));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function getFarms($farm_id = null) {
        try {
            $repo = $this->em->getRepository('Entity\Farm');
            $qb = $repo->createQueryBuilder('f');

            $farms = array();

            if (!empty($farm_id)) {
                $qb->select('f.id', 'IDENTITY (f.gallery) AS gallery_id', 'IDENTITY (f.region) AS region_id', 'f.status', 'f.name', 'f.lat', 'f.lon', 'f.description', 'f.ratings_number', 'f.rating', 'f.create_date', 'r.name AS regionname', 'i.filename');
                $qb->leftJoin('f.region', 'r');
                $qb->leftJoin('f.gallery', 'g');
                $qb->leftJoin('g.images', 'i', 'WITH', 'i.cover = 1');
                $qb->andWhere('f.id = :id');
                $qb->setParameter('id', $farm_id);
                $farms = $qb->getQuery()->getArrayResult();

                $repo = $this->em->getRepository('Entity\Product');
                $qb = $repo->createQueryBuilder('p');

                $qb->select('p.id', 'IDENTITY (p.farm) AS farm_id', 'IDENTITY (p.category) AS category_id', 'p.label');
                $qb->where('IDENTITY (p.farm) = :fid');
                $qb->setParameter('fid', $farms[0]['id']);
                $qb->orderBy('p.label', 'ASC');
                $products = $qb->getQuery()->getArrayResult();
                $labels = array();
                $distinct = array();
                foreach ($products as $p) {
                    $p['region_id'] = $farms[0]['region_id'];
                    if (!in_array($p['label'], $labels)) {
                        $labels[] = $p['label'];
                        $distinct[] = $p;
                    }
                }
                $farms[0]['products'] = $distinct;
            } else {
                $qb->select('f.id', 'IDENTITY (f.gallery) AS gallery_id', 'IDENTITY (f.region) AS region_id', 'f.status', 'f.name', 'f.ratings_number', 'f.rating', 'f.create_date', 'r.name AS regionname', 'i.filename');
                $qb->leftJoin('f.region', 'r');
                $qb->leftJoin('f.gallery', 'g');
                $qb->leftJoin('g.images', 'i', 'WITH', 'i.cover = 1');
                $qb->orderBy('f.name', 'ASC');
                $farms = $qb->getQuery()->getArrayResult();
            }

            echo json_encode(array('status' => true, 'items' => $farms));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function getFarmList() {
        try {
            $repo = $this->em->getRepository('Entity\Farm');
            $qb = $repo->createQueryBuilder('f');

            $qb->select('f.id', 'IDENTITY (f.region) AS region_id', 'f.status', 'f.name');
            $qb->leftJoin('f.region', 'r');

            $qb->orderBy('f.name', 'ASC');

            $farmlist = $qb->getQuery()->getArrayResult();

            echo json_encode(array('status' => true, 'items' => $farmlist));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

}
