(function () {
    'use strict';
    
    var GroceryApp = angular.module('GroceryApp');    

    //*** NAVIGATION DIRECTIVE ***//
    GroceryApp.directive('menuItems', MenuItems);
    function MenuItems() {
        //directive definition object
        var ddo = {
            restrict: 'E',
            templateUrl: 'src/app/templates/navigation.html',
            controller: 'NavController',
            controllerAs: 'nav'
        };
        return ddo;
    }

    //*** IDENTITY DIRECTIVE ***//
    GroceryApp.directive('userPanel', UserPanel);
    function UserPanel() {
        var ddo = {
            restrict: 'E',
            templateUrl: 'src/app/templates/login.html',
            controller: 'IdentityController',
            controllerAs: 'identity'
        };
        return ddo;
    }

    //*** CART DIRECTIVE ***//
    GroceryApp.directive('cartPanel', CartPanel);
    function CartPanel() {
        var ddo = {
            restrict: 'E',
            templateUrl: 'src/app/templates/cart.html',
            controller: 'RootController',
            controllerAs: 'root'
        };
        return ddo;
    }

    //*** PRODUCT LIST ITEM DIRECTIVE ***//
    GroceryApp.directive('productListItem', ProductListItem);
    function ProductListItem() {
        var ddo = {
            restrict: 'E',
            templateUrl: 'src/app/templates/product-list-item.html'
        };
        return ddo;
    }

    //*** FARM RATING DIRECTIVE ***//
    GroceryApp.directive('farmRating', FarmRating);
    function FarmRating() {
        var ddo = {
            restrict: 'E',
            templateUrl: 'src/app/templates/farm-rating.html'
        };
        return ddo;
    }
    
    //*** SPINNER DIRECTIVE ***//
    GroceryApp.directive('spinner', SpinnerDirective);    
    function SpinnerDirective() {        
        var ddo = {
            restrict: 'E',
            templateUrl: 'src/app/templates/spinner.html',
            controller: 'SpinnerController',
            controllerAs: 'spinnerctrl'
        };
        return ddo;
    }
    
    //*** BACK DIRECTIVE ***//
    GroceryApp.directive('backButton', function ($window) {
        return {
            restrict: 'E',
            templateUrl: 'src/app/templates/back.html',
            link: function (scope, elem, attrs) {
                elem.bind('click', function () {
                    $window.history.back();
                });
            }
        };
    });
    
    //*** FOCUSME DIRECTIVE ***//
    GroceryApp.directive('focusme', function ($timeout, $parse) {
        return {
            link: function (scope, elem, attrs) {
                var model = $parse(attrs.focusme);
                scope.$watch(model, function (value) {
                    if (value === true) {
                        $timeout(function () {
                            elem[0].focus();
                        }, 100);
                    }
                });
                elem.bind('blur', function () {
                    scope.$apply(function () {
                        scope.focussed = false;
                    });
                });
            }
        };
    });
    
    //*** OFF-CANVAS MENU DIRECTIVE ***//
    GroceryApp.directive("offCanvasMenu", function () {
        return {
            restrict: 'A',
            replace: false,
            link: function (scope, element) {
                scope.isMenuOpen = false;
                scope.toggleMenu = function () {
                    scope.isMenuOpen = !scope.isMenuOpen;
                };
                element.bind('click', function (e) {
                    if (e.target.tagName.toLowerCase() === 'a') {
                        scope.isMenuOpen = false;
                    }
                });
            }
        };
    });

})();