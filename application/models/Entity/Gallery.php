<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="galleries")
 */
class Gallery {

    /**
     * @Id
     * @Column(type="integer", nullable=false, options={"unsigned":true})
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Column(type="string", nullable=true, length=100)
     */
    protected $name;

    /**
     * @Column(type="string", nullable=false, length=10)
     */
    protected $type;

    /**
     * @OneToMany(targetEntity="Image", mappedBy="gallery")     
     */
    protected $images;

    public function __construct() {
        
    }

    function getId() {
        return $this->id;
    }

    function getName() {
        return $this->name;
    }

    function getType() {
        return $this->type;
    }

    function getImages() {
        return $this->images;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setName() {
        return $this->name;
    }

    function setType($type) {
        $this->type = $type;
    }

    function setImages($images) {
        $this->images = $images;
    }

}
