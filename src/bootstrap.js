
'use strict';

var userdata;
var categories;
var regions;

var GroceryApp = angular.module('GroceryApp', ['ngRoute', 'ngStorage', 'ui.bootstrap']);

function fetchData() {
    var initInjector = angular.injector(["ng"]);
    var $http = initInjector.get("$http");
    var $q = initInjector.get("$q");

    var spinner = document.createElement("img");
    spinner.src = "spinner.gif";
    spinner.id = "spinner";
    document.body.appendChild(spinner);

    var request0 = $http.get(base_url + "identity/checkIfLoggedIn");
    var request1 = $http.get(base_url + "grocery/getProductCategories");
    var request2 = $http.get(base_url + "grocery/getRegions");

    return $q.all([request0, request1, request2]).then(function (results) {
        userdata = results[0].data;
        categories = results[1].data.items;
        regions = results[2].data.items;
    })
            .catch(function (error) {
                console.log("Error:", error);
            })
            .finally(function () {
                spinner.remove();
            });
}

function bootstrapApplication(userdata, categories, regions) {
    angular.element(document).ready(function () {
        angular.bootstrap(document, ["GroceryApp"]);
    });
}

/***************************************************************************
 BOOTSTRAP APPLICATION
 ***************************************************************************/
fetchData().then(function () {
    bootstrapApplication(userdata, categories, regions);
});

