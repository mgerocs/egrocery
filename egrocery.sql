-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Gép: 127.0.0.1
-- Létrehozás ideje: 2016. Nov 04. 16:11
-- Kiszolgáló verziója: 10.1.9-MariaDB
-- PHP verzió: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `egrocery`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('d2e6286d23433b689fa386e1f8b266e82b9d9ad1', '::1', 1478098743, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383039383632343b69735f6c6f676765645f696e7c623a303b),
('78671ad80f524904d0909ba247eaf342d70ade5c', '::1', 1478099665, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383039393337343b69735f6c6f676765645f696e7c623a303b),
('7db9c9fee3197179b8e23d4e7d728ddd562932d8', '::1', 1478100034, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383039393734373b69735f6c6f676765645f696e7c623a303b),
('d0aa86317aaa10016d29e5142430746772c86d49', '::1', 1478100472, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383130303138373b69735f6c6f676765645f696e7c623a303b),
('c3923068b30785c6ef7f9910a50284419c40d426', '::1', 1478100862, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383130303536363b69735f6c6f676765645f696e7c623a303b),
('038a4f51cd6c0c0a69d95cd5bb147abe65895878', '::1', 1478101151, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383130303838373b69735f6c6f676765645f696e7c623a303b),
('a39395705fb1cffe2a17bb3b02b380b68de43d74', '::1', 1478101197, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383130313139363b69735f6c6f676765645f696e7c623a303b),
('e7cbba12604ac2f79639d93eab969450293bf8ea', '::1', 1478101499, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383130313439383b69735f6c6f676765645f696e7c623a303b),
('3a3677e52eaa79dc935469fbf85b1eb0dca497b9', '::1', 1478102106, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383130313833343b69735f6c6f676765645f696e7c623a303b),
('4c3c151b647578cefc16c7b0f02f56230fa9c4b5', '::1', 1478179088, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383137393036313b69735f6c6f676765645f696e7c623a303b),
('8068ad50141cf99520161485948fd212dde3d486', '::1', 1478179695, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383137393435353b69735f6c6f676765645f696e7c623a303b),
('6ffbb5567ef5503df9f9d0b71214bf883f92e21b', '::1', 1478179871, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383137393738393b69735f6c6f676765645f696e7c623a303b),
('2f63b523d4638a52112f3514ceb8a1e13b8e0cba', '::1', 1478180386, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383138303131353b69735f6c6f676765645f696e7c623a303b),
('985f3d5ea1ddeec9438987c98bed27eeec0cc3d0', '::1', 1478180818, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383138303632373b69735f6c6f676765645f696e7c623a303b),
('15253d65bc968f68a55bf013f01c9878d1c74e89', '::1', 1478181278, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383138303939303b69735f6c6f676765645f696e7c623a303b),
('dd9966179c5f11c31ff2b90d28509344058cd59f', '::1', 1478181810, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383138313533333b69735f6c6f676765645f696e7c623a303b),
('bcb9623bc630ffd33a1ae133821c612427b26090', '::1', 1478182824, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383138323537363b69735f6c6f676765645f696e7c623a303b),
('9ebb6ea49bd3a5223e5351e3f34b5fa26310b570', '::1', 1478183161, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383138323933383b69735f6c6f676765645f696e7c623a303b),
('4bc84292247708d498bf8632ba9c4e61b3bf94e6', '::1', 1478183462, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383138333334373b69735f6c6f676765645f696e7c623a303b),
('068e5a1d25335c10aba96a647509d299e45e6328', '::1', 1478183804, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383138333638393b69735f6c6f676765645f696e7c623a303b),
('688b0a37ea8ee43b6cb563de02a44636782f485a', '::1', 1478184308, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383138343131313b69735f6c6f676765645f696e7c623a303b),
('a95ea2454a1eb5a61adb0099b0677524b2db7db5', '::1', 1478184660, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383138343432333b69735f6c6f676765645f696e7c623a303b),
('18b72d094c49cf065bc00ea6b21a2903f435f94c', '::1', 1478185317, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383138353033333b69735f6c6f676765645f696e7c623a303b),
('2ec8636dc42a284e54d78afb81fecc8936b6b85a', '::1', 1478185671, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383138353430323b69735f6c6f676765645f696e7c623a303b),
('f777689e2348319156ec895979d6d11220cc1113', '::1', 1478256210, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383235363230343b69735f6c6f676765645f696e7c623a303b),
('666574e9d87b82f1aa5c3087631a5bf5b7849818', '::1', 1478256634, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383235363633343b69735f6c6f676765645f696e7c623a303b),
('ce405c56cc12bdc8c079bf00b0de630bbcee9331', '::1', 1478256635, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383235363633353b69735f6c6f676765645f696e7c623a303b),
('9574aabc82c88a32efe79802af5b67911f4d4dbc', '::1', 1478256635, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383235363633353b69735f6c6f676765645f696e7c623a303b),
('f15afc25c9d518cc066f8996acd25182d2d0c511', '::1', 1478256635, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383235363633353b69735f6c6f676765645f696e7c623a303b),
('b69f9804aa4c2e6835ec1a4b09b2d67c7d54003c', '::1', 1478256635, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383235363633353b69735f6c6f676765645f696e7c623a303b),
('7522a39c0d72f0a07bff1e1cb8bd20800029aecb', '::1', 1478256947, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383235363731303b69735f6c6f676765645f696e7c623a303b),
('ff9a41f4ca9cebc9cd67e9b8837e0e7219e03d16', '::1', 1478257166, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383235373032353b69735f6c6f676765645f696e7c623a303b),
('4b10b35fdcdca7c34e3a79d57ba87d04bfb25f9b', '::1', 1478257703, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383235373435363b69735f6c6f676765645f696e7c623a303b),
('e80fe35d885bf333e3bc5515c1c4d8098c18d680', '::1', 1478258026, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383235373830333b69735f6c6f676765645f696e7c623a303b),
('9821fdc76673a381563daccf78060a73b84613da', '::1', 1478258445, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383235383137383b69735f6c6f676765645f696e7c623a303b),
('0edc35cce48f6e1e18a7131698589190eb9148ed', '::1', 1478258492, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383235383439303b69735f6c6f676765645f696e7c623a303b),
('458b747af11d478cf79664e3ac6d43b0ea74798a', '::1', 1478262479, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383236323235313b69735f6c6f676765645f696e7c623a303b),
('5f5ef4f1bffbc965674ab43c9fb69a4accdeb322', '::1', 1478262742, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383236323537393b69735f6c6f676765645f696e7c623a303b),
('ff94bc7d838244134d1afd19bf376603824e3a6e', '::1', 1478263234, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383236323939363b69735f6c6f676765645f696e7c623a303b),
('932d1bb0477cd6807aebf5b84a203910e1d8334b', '::1', 1478263621, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383236333332313b69735f6c6f676765645f696e7c623a303b),
('d43edf593fd0c834d572256e8600674ff15893f8', '::1', 1478263838, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383236333633313b69735f6c6f676765645f696e7c623a303b),
('367fb85e8008360ddc9d9f5289cccd7c0d9eb4b4', '::1', 1478264254, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383236333935353b69735f6c6f676765645f696e7c623a303b),
('5d0b3e225c40e50b002b2673b3f4a10e360d6e48', '::1', 1478264715, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383236343731353b69735f6c6f676765645f696e7c623a303b),
('b0bdf85c0eda4296b194817784b134907bdfcfb1', '::1', 1478264715, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383236343731353b69735f6c6f676765645f696e7c623a303b),
('14adf69c0f26f714b900de550dd774172d46549a', '::1', 1478264997, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383236343731353b69735f6c6f676765645f696e7c623a303b),
('77bb16b414ac819b2dbd65f60361476a6a3ef168', '::1', 1478265146, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383236353134363b69735f6c6f676765645f696e7c623a303b),
('c8e8c4bbf67d830a1c17eb27d191d70754ec0c9d', '::1', 1478265196, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383236353134363b69735f6c6f676765645f696e7c623a303b),
('dd7aa9152d0dd6977ff87093a19666de7c38e675', '::1', 1478265557, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383236353530383b69735f6c6f676765645f696e7c623a303b),
('cc98c76ec331cfb66d628bbb9e954ec7801e5ae8', '::1', 1478266123, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383236353932323b69735f6c6f676765645f696e7c623a303b),
('2ed740a9c2ff2d65b72082fa22b35c5dc9501cc7', '::1', 1478266273, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383236363237333b69735f6c6f676765645f696e7c623a303b),
('9338897b3e4fffffed2edc5f8b4b0949884c8a39', '::1', 1478266273, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383236363237333b69735f6c6f676765645f696e7c623a303b),
('1526dc229a8a2ac46f118c7945dc8765f7d6b24e', '::1', 1478266483, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383236363237333b69735f6c6f676765645f696e7c623a303b),
('6f7003fd1a813ed11ac52bab6f25a5667bf4c263', '::1', 1478266778, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383236363737383b69735f6c6f676765645f696e7c623a303b),
('b04bdc0859069377960a5cc337ee2788c210269c', '::1', 1478266778, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383236363737383b69735f6c6f676765645f696e7c623a303b),
('f0aafd97b7236fa62c373eee22c7a43fc908d4b8', '::1', 1478267043, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383236363737383b69735f6c6f676765645f696e7c623a303b),
('e9fc97dbc9052bdee6d57648fbcde2ca2f1b9d95', '::1', 1478267442, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383236373137323b69735f6c6f676765645f696e7c623a303b),
('f81e035940e761d85700fc9a7e7f6e7a6d24340c', '::1', 1478267530, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383236373533303b69735f6c6f676765645f696e7c623a303b),
('db77b5d0661e5e62a68354cd95564adad468068b', '::1', 1478267592, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383236373533303b69735f6c6f676765645f696e7c623a303b),
('a94c9368bc3e09db8639d7df98ec22226b02201c', '::1', 1478267530, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383236373533303b69735f6c6f676765645f696e7c623a303b),
('019627cfb7f0e5bafb67d7b6f1880e0a9329a321', '::1', 1478268115, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383236373834393b69735f6c6f676765645f696e7c623a303b),
('caff981d26b01955852ad18ac4318206a6b89f5e', '::1', 1478268407, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383236383139333b69735f6c6f676765645f696e7c623a303b),
('676387f38518f888a11613f246a691a0d8ce60f8', '::1', 1478268652, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383236383531333b69735f6c6f676765645f696e7c623a303b),
('e1c99de3182a1a2256f50ba0640a27541ed14464', '::1', 1478268815, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383236383831353b69735f6c6f676765645f696e7c623a303b),
('ef30d517995097c8d6a5603f7d10e48147f37b6e', '::1', 1478269101, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383236383831353b69735f6c6f676765645f696e7c623a303b),
('a2fc35383959c8afd630ef58533626eae9a21002', '::1', 1478269337, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383236393133323b69735f6c6f676765645f696e7c623a303b),
('8439138610335ae5816cf0c6c1801d133f0e1020', '::1', 1478269739, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383236393733393b69735f6c6f676765645f696e7c623a303b),
('774173731db0a0f53a1dd6d96a078343a46e3cf4', '::1', 1478269739, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383236393733393b69735f6c6f676765645f696e7c623a303b),
('4e9c6dee4c3f0f5bc5b3e24ba8721893d2f07e80', '::1', 1478269977, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383236393733393b69735f6c6f676765645f696e7c623a303b),
('ba6d239af33a4f6a85a0313181d72b9f57cba34c', '::1', 1478270266, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383237303036373b69735f6c6f676765645f696e7c623a303b),
('dac6371b988463ccc98f77a2a2b9766e0989941f', '::1', 1478270705, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383237303430353b69735f6c6f676765645f696e7c623a303b),
('045d2b1c028ad45989732dce6a83979cd96c7758', '::1', 1478270976, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383237303730363b69735f6c6f676765645f696e7c623a303b),
('b2c742029436b9da666e51783f02c49d51ae7202', '::1', 1478271318, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383237313031393b69735f6c6f676765645f696e7c623a303b),
('a7562380a6c6a5624b3e9fff45365d0c1d2522ca', '::1', 1478271557, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383237313332323b69735f6c6f676765645f696e7c623a303b),
('81c162e8194d265510a85d9d6360b9e1e0a8319c', '::1', 1478271973, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383237313732333b69735f6c6f676765645f696e7c623a303b),
('5f0e45a0c90de0977f1ee27220f6716625ff0a5d', '::1', 1478272170, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383237323033323b69735f6c6f676765645f696e7c623a303b);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `farms`
--

CREATE TABLE `farms` (
  `id` int(10) UNSIGNED NOT NULL,
  `gallery_id` int(10) UNSIGNED DEFAULT NULL,
  `region_id` int(10) UNSIGNED DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `create_date` datetime NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lat` decimal(9,6) DEFAULT NULL,
  `lon` decimal(9,6) DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(3000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ratings_number` int(10) UNSIGNED NOT NULL,
  `rating` decimal(4,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `farms`
--

INSERT INTO `farms` (`id`, `gallery_id`, `region_id`, `status`, `create_date`, `name`, `location`, `lat`, `lon`, `email`, `description`, `ratings_number`, `rating`) VALUES
(1, 91, 6, 1, '2016-05-31 00:00:00', 'Tánczos Gyümölcsöskert', 'Páka', '47.274440', '18.490780', 'tanczos.gyumolcskert@email.hu', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consectetur, quam id vestibulum consectetur, elit eros porta turpis, vel tincidunt neque ipsum ac augue. Phasellus porta ligula neque, eu pharetra lacus pulvinar ac. Nunc gravida mi ac leo consequat, ut scelerisque ante posuere. Morbi tellus lectus, tristique sit amet fringilla eu, ornare lacinia lectus. Nullam interdum egestas elit vel sollicitudin. Curabitur quis felis malesuada, cursus eros at, imperdiet sem. Nam ut interdum neque. Mauris quis libero in lacus ullamcorper viverra. Integer lacinia vitae dui id sagittis. Mauris viverra pharetra turpis vel gravida. In id metus eleifend, euismod odio sit amet, rhoncus ligula. ', 123, '4.20'),
(2, 92, 8, 1, '2016-05-31 00:00:00', 'Jótermés Gyümölcsös', 'Derecske', '47.353690', '21.571780', 'jotermes.gyumolcsos@email.hu', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consectetur, quam id vestibulum consectetur, elit eros porta turpis, vel tincidunt neque ipsum ac augue. Phasellus porta ligula neque, eu pharetra lacus pulvinar ac. Nunc gravida mi ac leo consequat, ut scelerisque ante posuere. Morbi tellus lectus, tristique sit amet fringilla eu, ornare lacinia lectus. Nullam interdum egestas elit vel sollicitudin. Curabitur quis felis malesuada, cursus eros at, imperdiet sem. Nam ut interdum neque. Mauris quis libero in lacus ullamcorper viverra. Integer lacinia vitae dui id sagittis. Mauris viverra pharetra turpis vel gravida. In id metus eleifend, euismod odio sit amet, rhoncus ligula. ', 89, '4.67'),
(3, 93, 12, 1, '2016-05-31 00:00:00', 'Hajdú Kálmán', 'Erdőkürt', '47.772850', '19.457281', 'hajdu.kalman@email.hu', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consectetur, quam id vestibulum consectetur, elit eros porta turpis, vel tincidunt neque ipsum ac augue. Phasellus porta ligula neque, eu pharetra lacus pulvinar ac. Nunc gravida mi ac leo consequat, ut scelerisque ante posuere. Morbi tellus lectus, tristique sit amet fringilla eu, ornare lacinia lectus. Nullam interdum egestas elit vel sollicitudin. Curabitur quis felis malesuada, cursus eros at, imperdiet sem. Nam ut interdum neque. Mauris quis libero in lacus ullamcorper viverra. Integer lacinia vitae dui id sagittis. Mauris viverra pharetra turpis vel gravida. In id metus eleifend, euismod odio sit amet, rhoncus ligula. ', 214, '3.34'),
(4, 94, 1, 1, '2016-05-31 00:00:00', 'Jóudvar Biokertészet', 'Soltvadkert', '46.580870', '19.393700', 'joudvar.biokertezset@email.hu', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consectetur, quam id vestibulum consectetur, elit eros porta turpis, vel tincidunt neque ipsum ac augue. Phasellus porta ligula neque, eu pharetra lacus pulvinar ac. Nunc gravida mi ac leo consequat, ut scelerisque ante posuere. Morbi tellus lectus, tristique sit amet fringilla eu, ornare lacinia lectus. Nullam interdum egestas elit vel sollicitudin. Curabitur quis felis malesuada, cursus eros at, imperdiet sem. Nam ut interdum neque. Mauris quis libero in lacus ullamcorper viverra. Integer lacinia vitae dui id sagittis. Mauris viverra pharetra turpis vel gravida. In id metus eleifend, euismod odio sit amet, rhoncus ligula. ', 45, '4.83'),
(5, 95, 17, 1, '2016-05-31 00:00:00', 'Ka Pál', 'Velemér', '46.739371', '16.377800', 'ka.pal@email.hu', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consectetur, quam id vestibulum consectetur, elit eros porta turpis, vel tincidunt neque ipsum ac augue. Phasellus porta ligula neque, eu pharetra lacus pulvinar ac. Nunc gravida mi ac leo consequat, ut scelerisque ante posuere. Morbi tellus lectus, tristique sit amet fringilla eu, ornare lacinia lectus. Nullam interdum egestas elit vel sollicitudin. Curabitur quis felis malesuada, cursus eros at, imperdiet sem. Nam ut interdum neque. Mauris quis libero in lacus ullamcorper viverra. Integer lacinia vitae dui id sagittis. Mauris viverra pharetra turpis vel gravida. In id metus eleifend, euismod odio sit amet, rhoncus ligula. ', 5, '3.13'),
(6, 96, 10, 1, '2016-05-31 00:00:00', 'Kertészné Aranyos Piroska', 'Cserkesőlő', '46.863889', '20.205556', 'kapiroska@email.hu', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consectetur, quam id vestibulum consectetur, elit eros porta turpis, vel tincidunt neque ipsum ac augue. Phasellus porta ligula neque, eu pharetra lacus pulvinar ac. Nunc gravida mi ac leo consequat, ut scelerisque ante posuere. Morbi tellus lectus, tristique sit amet fringilla eu, ornare lacinia lectus. Nullam interdum egestas elit vel sollicitudin. Curabitur quis felis malesuada, cursus eros at, imperdiet sem. Nam ut interdum neque. Mauris quis libero in lacus ullamcorper viverra. Integer lacinia vitae dui id sagittis. Mauris viverra pharetra turpis vel gravida. In id metus eleifend, euismod odio sit amet, rhoncus ligula. ', 221, '4.00'),
(7, 97, 10, 1, '2016-05-31 00:00:00', 'Pattantyús Özséb', 'Zagyvarékas', '47.266000', '20.125000', 'pattozseb@email.hu', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consectetur, quam id vestibulum consectetur, elit eros porta turpis, vel tincidunt neque ipsum ac augue. Phasellus porta ligula neque, eu pharetra lacus pulvinar ac. Nunc gravida mi ac leo consequat, ut scelerisque ante posuere. Morbi tellus lectus, tristique sit amet fringilla eu, ornare lacinia lectus. Nullam interdum egestas elit vel sollicitudin. Curabitur quis felis malesuada, cursus eros at, imperdiet sem. Nam ut interdum neque. Mauris quis libero in lacus ullamcorper viverra. Integer lacinia vitae dui id sagittis. Mauris viverra pharetra turpis vel gravida. In id metus eleifend, euismod odio sit amet, rhoncus ligula. ', 178, '3.35'),
(8, 98, 9, 1, '2016-05-31 00:00:00', 'Csintalan Sajtműhely', 'Eger', '47.902210', '20.376290', 'csintalan.sajtmuhely@email.hu', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consectetur, quam id vestibulum consectetur, elit eros porta turpis, vel tincidunt neque ipsum ac augue. Phasellus porta ligula neque, eu pharetra lacus pulvinar ac. Nunc gravida mi ac leo consequat, ut scelerisque ante posuere. Morbi tellus lectus, tristique sit amet fringilla eu, ornare lacinia lectus. Nullam interdum egestas elit vel sollicitudin. Curabitur quis felis malesuada, cursus eros at, imperdiet sem. Nam ut interdum neque. Mauris quis libero in lacus ullamcorper viverra. Integer lacinia vitae dui id sagittis. Mauris viverra pharetra turpis vel gravida. In id metus eleifend, euismod odio sit amet, rhoncus ligula. ', 56, '3.79'),
(9, 99, 16, 1, '2016-05-31 00:00:00', 'Nagy Családi Gazdaság', 'Szekszárd', '46.347034', '18.702100', 'ncsg@email.hu', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consectetur, quam id vestibulum consectetur, elit eros porta turpis, vel tincidunt neque ipsum ac augue. Phasellus porta ligula neque, eu pharetra lacus pulvinar ac. Nunc gravida mi ac leo consequat, ut scelerisque ante posuere. Morbi tellus lectus, tristique sit amet fringilla eu, ornare lacinia lectus. Nullam interdum egestas elit vel sollicitudin. Curabitur quis felis malesuada, cursus eros at, imperdiet sem. Nam ut interdum neque. Mauris quis libero in lacus ullamcorper viverra. Integer lacinia vitae dui id sagittis. Mauris viverra pharetra turpis vel gravida. In id metus eleifend, euismod odio sit amet, rhoncus ligula. ', 100, '4.08'),
(10, 100, 7, 1, '2016-05-31 00:00:00', 'Baráth Iván', 'Szany', '47.600000', '17.200000', 'barath.ivan@email.hu', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consectetur, quam id vestibulum consectetur, elit eros porta turpis, vel tincidunt neque ipsum ac augue. Phasellus porta ligula neque, eu pharetra lacus pulvinar ac. Nunc gravida mi ac leo consequat, ut scelerisque ante posuere. Morbi tellus lectus, tristique sit amet fringilla eu, ornare lacinia lectus. Nullam interdum egestas elit vel sollicitudin. Curabitur quis felis malesuada, cursus eros at, imperdiet sem. Nam ut interdum neque. Mauris quis libero in lacus ullamcorper viverra. Integer lacinia vitae dui id sagittis. Mauris viverra pharetra turpis vel gravida. In id metus eleifend, euismod odio sit amet, rhoncus ligula. ', 63, '4.25'),
(11, 101, 14, 1, '2016-05-31 00:00:00', 'Karátos Méhészet', 'Patca', '46.285080', '17.723630', 'karatos.meheszet@email.hu', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consectetur, quam id vestibulum consectetur, elit eros porta turpis, vel tincidunt neque ipsum ac augue. Phasellus porta ligula neque, eu pharetra lacus pulvinar ac. Nunc gravida mi ac leo consequat, ut scelerisque ante posuere. Morbi tellus lectus, tristique sit amet fringilla eu, ornare lacinia lectus. Nullam interdum egestas elit vel sollicitudin. Curabitur quis felis malesuada, cursus eros at, imperdiet sem. Nam ut interdum neque. Mauris quis libero in lacus ullamcorper viverra. Integer lacinia vitae dui id sagittis. Mauris viverra pharetra turpis vel gravida. In id metus eleifend, euismod odio sit amet, rhoncus ligula. ', 11, '4.68'),
(12, 102, 8, 1, '2016-05-31 00:00:00', 'Kendermag Háztáji Gazdaság', 'Bojt', '47.193200', '21.731500', 'khg@email.hu', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consectetur, quam id vestibulum consectetur, elit eros porta turpis, vel tincidunt neque ipsum ac augue. Phasellus porta ligula neque, eu pharetra lacus pulvinar ac. Nunc gravida mi ac leo consequat, ut scelerisque ante posuere. Morbi tellus lectus, tristique sit amet fringilla eu, ornare lacinia lectus. Nullam interdum egestas elit vel sollicitudin. Curabitur quis felis malesuada, cursus eros at, imperdiet sem. Nam ut interdum neque. Mauris quis libero in lacus ullamcorper viverra. Integer lacinia vitae dui id sagittis. Mauris viverra pharetra turpis vel gravida. In id metus eleifend, euismod odio sit amet, rhoncus ligula. \r\n', 191, '4.56');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `galleries`
--

CREATE TABLE `galleries` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `galleries`
--

INSERT INTO `galleries` (`id`, `name`, `type`) VALUES
(1, '﻿alma', 'product'),
(2, 'alma', 'product'),
(3, 'zöldalma', 'product'),
(4, 'vilmoskörte', 'product'),
(5, 'vilmoskörte', 'product'),
(6, 'őszibarack', 'product'),
(7, 'őszibarack', 'product'),
(8, 'őszibarack', 'product'),
(9, 'sárgabarack', 'product'),
(10, 'sárgabarack', 'product'),
(11, 'sárgabarack', 'product'),
(12, 'szilva', 'product'),
(13, 'szilva', 'product'),
(14, 'dió', 'product'),
(15, 'dió', 'product'),
(16, 'dió', 'product'),
(17, 'görögdinnye', 'product'),
(18, 'görögdinnye', 'product'),
(19, 'sárgadinnye', 'product'),
(20, 'mák', 'product'),
(21, 'mák', 'product'),
(22, 'fokhagyma', 'product'),
(23, 'fokhagyma', 'product'),
(24, 'fokhagyma', 'product'),
(25, 'fokhagyma', 'product'),
(26, 'fehérrépa', 'product'),
(27, 'fehérrépa', 'product'),
(28, 'fehérrépa', 'product'),
(29, 'csiperke gomba', 'product'),
(30, 'vargánya', 'product'),
(31, 'fejes káposzta', 'product'),
(32, 'fejes káposzta', 'product'),
(33, 'kelkáposzta', 'product'),
(34, 'karalábé', 'product'),
(35, 'karalábé', 'product'),
(36, 'karalábé', 'product'),
(37, 'lilahagyma', 'product'),
(38, 'lilahagyma', 'product'),
(39, 'lilahagyma', 'product'),
(40, 'vöröshagyma', 'product'),
(41, 'vöröshagyma', 'product'),
(42, 'vöröshagyma', 'product'),
(43, 'paradicsom', 'product'),
(44, 'paradicsom', 'product'),
(45, 'paradicsom', 'product'),
(46, 'paradicsom', 'product'),
(47, 'TV paprika', 'product'),
(48, 'TV paprika', 'product'),
(49, 'erős paprika', 'product'),
(50, 'fejes saláta', 'product'),
(51, 'fejes saláta', 'product'),
(52, 'fejes saláta', 'product'),
(53, 'sárgarépa', 'product'),
(54, 'sárgarépa', 'product'),
(55, 'sárgarépa', 'product'),
(56, 'zöldbab', 'product'),
(57, 'zeller', 'product'),
(58, 'póréhagyma', 'product'),
(59, 'tarka bab', 'product'),
(60, 'málna', 'product'),
(61, 'aszalt áfonya', 'product'),
(62, 'málna', 'product'),
(63, 'medvehagyma', 'product'),
(64, 'petrezselyem', 'product'),
(65, 'petrezselyem', 'product'),
(66, 'tehéntej', 'product'),
(67, 'tehéntej', 'product'),
(68, 'tehéntej', 'product'),
(69, 'kecsketej', 'product'),
(70, 'kecsketej', 'product'),
(71, 'kecsketej', 'product'),
(72, 'sajt', 'product'),
(73, 'kecskesajt', 'product'),
(74, 'juhsajt', 'product'),
(75, 'füstölt sajt', 'product'),
(76, 'sajt', 'product'),
(77, 'túró', 'product'),
(78, 'túró', 'product'),
(79, 'tyúktojás', 'product'),
(80, 'tyúktojás', 'product'),
(81, 'tyúktojás', 'product'),
(82, 'tyúktojás', 'product'),
(83, 'málnalekvár', 'product'),
(84, 'meggylekvár', 'product'),
(85, 'akácméz', 'product'),
(86, 'virágméz', 'product'),
(87, 'repceméz', 'product'),
(88, 'virágméz', 'product'),
(89, 'virágméz', 'product'),
(90, 'csemege kukorica', 'product'),
(91, 'Tánczos Gyümölcsöskert', 'farm'),
(92, 'Jótermés Gyümölcsös', 'farm'),
(93, 'Hajdú Kálmán', 'farm'),
(94, 'Jóudvar Biokertészet', 'farm'),
(95, 'Ka Pál', 'farm'),
(96, 'Kertészné Aranyos Piroska', 'farm'),
(97, 'Pattantyús Özséb', 'farm'),
(98, 'Csintalan Sajtműhely', 'farm'),
(99, 'Nagy Családi Gazdaság', 'farm'),
(100, 'Baráth Iván', 'farm'),
(101, 'Karátos Méhészet', 'farm'),
(102, 'Kendermag Háztáji Gazdaság', 'farm\r\n');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `images`
--

CREATE TABLE `images` (
  `id` int(10) UNSIGNED NOT NULL,
  `gallery_id` int(10) UNSIGNED DEFAULT NULL,
  `filename` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ext` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `size` int(10) UNSIGNED NOT NULL,
  `cover` tinyint(1) NOT NULL,
  `weight` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `images`
--

INSERT INTO `images` (`id`, `gallery_id`, `filename`, `ext`, `size`, `cover`, `weight`) VALUES
(1, 1, 'alma1.png', '.png', 0, 1, 1),
(2, 2, 'alma2.png', '.png', 0, 1, 1),
(3, 3, 'alma3.png', '.png', 0, 1, 1),
(4, 4, 'korte1.png', '.png', 0, 1, 1),
(5, 5, 'korte2.png', '.png', 0, 1, 1),
(6, 6, 'oszibarack1.png', '.png', 0, 1, 1),
(7, 7, 'oszibarack2.png', '.png', 0, 1, 1),
(8, 8, 'oszibarack3.png', '.png', 0, 1, 1),
(9, 9, 'sargabarack1.png', '.png', 0, 1, 1),
(10, 10, 'sargabarack2.png', '.png', 0, 1, 1),
(11, 11, 'sargabarack3.png', '.png', 0, 1, 1),
(12, 12, 'szilva1.png', '.png', 0, 1, 1),
(13, 13, 'szilva2.png', '.png', 0, 1, 1),
(14, 14, 'dio1.png', '.png', 0, 1, 1),
(15, 15, 'dio2.png', '.png', 0, 1, 1),
(16, 16, 'dio3.png', '.png', 0, 1, 1),
(17, 17, 'dinnye1.png', '.png', 0, 1, 1),
(18, 18, 'dinnye2.png', '.png', 0, 1, 1),
(19, 19, 'dinnye3.png', '.png', 0, 1, 1),
(20, 20, 'mak2.png', '.png', 0, 1, 1),
(21, 21, 'mak1.png', '.png', 0, 1, 1),
(22, 22, 'fokhagyma1.png', '.png', 0, 1, 1),
(23, 23, 'fokhagyma2.png', '.png', 0, 1, 1),
(24, 24, 'fokhagyma3.png', '.png', 0, 1, 1),
(25, 25, 'fokhagyma4.png', '.png', 0, 1, 1),
(26, 26, 'feherrepa1.png', '.png', 0, 1, 1),
(27, 27, 'feherrepa2.png', '.png', 0, 1, 1),
(28, 28, 'feherrepa3.png', '.png', 0, 1, 1),
(29, 29, 'gomba1.png', '.png', 0, 1, 1),
(30, 30, 'gomba2.png', '.png', 0, 1, 1),
(31, 31, 'kaposzta1.png', '.png', 0, 1, 1),
(32, 32, 'kaposzta2.png', '.png', 0, 1, 1),
(33, 33, 'kaposzta3.png', '.png', 0, 1, 1),
(34, 34, 'karalabe1.png', '.png', 0, 1, 1),
(35, 35, 'karalabe2.png', '.png', 0, 1, 1),
(36, 36, 'karalabe3.png', '.png', 0, 1, 1),
(37, 37, 'lilahagyma1.png', '.png', 0, 1, 1),
(38, 38, 'lilahagyma2.png', '.png', 0, 1, 1),
(39, 39, 'lilahagyma3.png', '.png', 0, 1, 1),
(40, 40, 'voroshagyma1.png', '.png', 0, 1, 1),
(41, 41, 'voroshagyma2.png', '.png', 0, 1, 1),
(42, 42, 'voroshagyma3.png', '.png', 0, 1, 1),
(43, 43, 'paradicsom1.png', '.png', 0, 1, 1),
(44, 44, 'paradicsom2.png', '.png', 0, 1, 1),
(45, 45, 'paradicsom3.png', '.png', 0, 1, 1),
(46, 46, 'paradicsom4.png', '.png', 0, 1, 1),
(47, 47, 'paprika1.png', '.png', 0, 1, 1),
(48, 48, 'paprika1.png', '.png', 0, 1, 1),
(49, 49, 'paprika2.png', '.png', 0, 1, 1),
(50, 50, 'salata1.png', '.png', 0, 1, 1),
(51, 51, 'salata2.png', '.png', 0, 1, 1),
(52, 52, 'salata3.png', '.png', 0, 1, 1),
(53, 53, 'sargarepa1.png', '.png', 0, 1, 1),
(54, 54, 'sargarepa2.png', '.png', 0, 1, 1),
(55, 55, 'sargarepa3.png', '.png', 0, 1, 1),
(56, 56, 'zoldbab.png', '.png', 0, 1, 1),
(57, 57, 'zeller.png', '.png', 0, 1, 1),
(58, 58, 'porehagyma.png', '.png', 0, 1, 1),
(59, 59, 'bab.png', '.png', 0, 1, 1),
(60, 60, 'malna1.png', '.png', 0, 1, 1),
(61, 61, 'aszaltafonya.png', '.png', 0, 1, 1),
(62, 62, 'malna2.png', '.png', 0, 1, 1),
(63, 63, 'medvehagyma.png', '.png', 0, 1, 1),
(64, 64, 'petrezselyem1.png', '.png', 0, 1, 1),
(65, 65, 'petrezselyem2.png', '.png', 0, 1, 1),
(66, 66, 'tehentej1.png', '.png', 0, 1, 1),
(67, 67, 'tehentej1.png', '.png', 0, 1, 1),
(68, 68, 'tehentej2.png', '.png', 0, 1, 1),
(69, 69, 'kecsketej1.png', '.png', 0, 1, 1),
(70, 70, 'kecsketej2.png', '.png', 0, 1, 1),
(71, 71, 'kecsketej3.png', '.png', 0, 1, 1),
(72, 72, 'sajt1.png', '.png', 0, 1, 1),
(73, 73, 'sajt2.png', '.png', 0, 1, 1),
(74, 74, 'juhsajt.png', '.png', 0, 1, 1),
(75, 75, 'sajt3.png', '.png', 0, 1, 1),
(76, 76, 'sajt4.png', '.png', 0, 1, 1),
(77, 77, 'turo1.png', '.png', 0, 1, 1),
(78, 78, 'turo2.png', '.png', 0, 1, 1),
(79, 79, 'tojas1.png', '.png', 0, 1, 1),
(80, 80, 'tojas2.png', '.png', 0, 1, 1),
(81, 81, 'tojas3.png', '.png', 0, 1, 1),
(82, 82, 'tojas4.png', '.png', 0, 1, 1),
(83, 83, 'malnalekvar.png', '.png', 0, 1, 1),
(84, 84, 'meggylekvar.png', '.png', 0, 1, 1),
(85, 85, 'mez1.png', '.png', 0, 1, 1),
(86, 86, 'mez2.png', '.png', 0, 1, 1),
(87, 87, 'mez4.png', '.png', 0, 1, 1),
(88, 88, 'mez3.png', '.png', 0, 1, 1),
(89, 89, 'mez5.png', '.png', 0, 1, 1),
(90, 90, 'kukorica.png', '.png', 0, 1, 1),
(91, 91, 'farm7.png', '.png', 0, 1, 1),
(92, 92, 'farm6.png', '.png', 0, 1, 1),
(93, 93, 'farm4.png', '.png', 0, 1, 1),
(94, 94, 'farm1.png', '.png', 0, 1, 1),
(95, 95, 'farm11.png', '.png', 0, 1, 1),
(96, 96, 'farm9.png', '.png', 0, 1, 1),
(97, 97, 'farm2.png', '.png', 0, 1, 1),
(98, 98, 'farm8.png', '.png', 0, 1, 1),
(99, 99, 'farm3.png', '.png', 0, 1, 1),
(100, 100, 'farm10.png', '.png', 0, 1, 1),
(101, 101, 'farm12.png', '.png', 0, 1, 1),
(102, 102, 'farm5.png', '.png', 0, 1, 1);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `farm_id` int(10) UNSIGNED DEFAULT NULL,
  `gallery_id` int(10) UNSIGNED DEFAULT NULL,
  `create_date` datetime NOT NULL,
  `status` tinyint(1) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `measure` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `products`
--

INSERT INTO `products` (`id`, `category_id`, `farm_id`, `gallery_id`, `create_date`, `status`, `price`, `measure`, `label`, `name`) VALUES
(1, 2, 1, 1, '2016-05-31 00:00:00', 1, '240', 'kiló', 'alma', 'alma'),
(2, 2, 2, 2, '2016-05-31 00:00:00', 1, '250', 'kiló', 'alma', 'alma'),
(3, 2, 3, 3, '2016-05-31 00:00:00', 1, '320', 'kiló', 'alma', 'zöldalma'),
(4, 2, 1, 4, '2016-05-31 00:00:00', 1, '360', 'kiló', 'körte', 'vilmoskörte'),
(5, 2, 2, 5, '2016-05-31 00:00:00', 1, '310', 'kiló', 'körte', 'vilmoskörte'),
(6, 2, 1, 6, '2016-05-31 00:00:00', 1, '420', 'kiló', 'barack', 'őszibarack'),
(7, 2, 2, 7, '2016-05-31 00:00:00', 1, '440', 'kiló', 'barack', 'őszibarack'),
(8, 2, 3, 8, '2016-05-31 00:00:00', 1, '415', 'kiló', 'barack', 'őszibarack'),
(9, 2, 1, 9, '2016-05-31 00:00:00', 1, '450', 'kiló', 'barack', 'sárgabarack'),
(10, 2, 2, 10, '2016-05-31 00:00:00', 1, '435', 'kiló', 'barack', 'sárgabarack'),
(11, 2, 3, 11, '2016-05-31 00:00:00', 1, '460', 'kiló', 'barack', 'sárgabarack'),
(12, 2, 2, 12, '2016-05-31 00:00:00', 1, '370', 'kiló', 'szilva', 'szilva'),
(13, 2, 3, 13, '2016-05-31 00:00:00', 1, '320', 'kiló', 'szilva', 'szilva'),
(14, 4, 1, 14, '2016-05-31 00:00:00', 1, '430', 'kiló', 'dió', 'dió'),
(15, 4, 12, 15, '2016-05-31 00:00:00', 1, '410', 'kiló', 'dió', 'dió'),
(16, 4, 7, 16, '2016-05-31 00:00:00', 1, '390', 'kiló', 'dió', 'dió'),
(17, 2, 10, 17, '2016-05-31 00:00:00', 1, '270', 'kiló', 'dinnye', 'görögdinnye'),
(18, 2, 5, 18, '2016-05-31 00:00:00', 1, '280', 'kiló', 'dinnye', 'görögdinnye'),
(19, 2, 5, 19, '2016-05-31 00:00:00', 1, '310', 'kiló', 'dinnye', 'sárgadinnye'),
(20, 4, 7, 20, '2016-05-31 00:00:00', 1, '830', 'kiló', 'mák', 'mák'),
(21, 4, 3, 21, '2016-05-31 00:00:00', 1, '790', 'kiló', 'mák', 'mák'),
(22, 1, 4, 22, '2016-05-31 00:00:00', 1, '220', 'csomag', 'hagyma', 'fokhagyma'),
(23, 1, 5, 23, '2016-05-31 00:00:00', 1, '275', 'csomag', 'hagyma', 'fokhagyma'),
(24, 1, 6, 24, '2016-05-31 00:00:00', 1, '240', 'csomag', 'hagyma', 'fokhagyma'),
(25, 1, 9, 25, '2016-05-31 00:00:00', 1, '290', 'csomag', 'hagyma', 'fokhagyma'),
(26, 1, 4, 26, '2016-05-31 00:00:00', 1, '200', 'csomag', 'répa', 'fehérrépa'),
(27, 1, 5, 27, '2016-05-31 00:00:00', 1, '230', 'csomag', 'répa', 'fehérrépa'),
(28, 1, 6, 28, '2016-05-31 00:00:00', 1, '215', 'csomag', 'répa', 'fehérrépa'),
(29, 1, 5, 29, '2016-05-31 00:00:00', 1, '315', 'kiló', 'gomba', 'csiperke gomba'),
(30, 1, 10, 30, '2016-05-31 00:00:00', 1, '520', 'kiló', 'gomba', 'vargánya'),
(31, 1, 4, 31, '2016-05-31 00:00:00', 1, '160', 'fej', 'káposzta', 'fejes káposzta'),
(32, 1, 5, 32, '2016-05-31 00:00:00', 1, '175', 'fej', 'káposzta', 'fejes káposzta'),
(33, 1, 6, 33, '2016-05-31 00:00:00', 1, '190', 'fej', 'káposzta', 'kelkáposzta'),
(34, 1, 4, 34, '2016-05-31 00:00:00', 1, '75', 'fej', 'karalábé', 'karalábé'),
(35, 1, 5, 35, '2016-05-31 00:00:00', 1, '85', 'fej', 'karalábé', 'karalábé'),
(36, 1, 6, 36, '2016-05-31 00:00:00', 1, '90', 'fej', 'karalábé', 'karalábé'),
(37, 1, 4, 37, '2016-05-31 00:00:00', 1, '135', 'csomag', 'hagyma', 'lilahagyma'),
(38, 1, 5, 38, '2016-05-31 00:00:00', 1, '180', 'csomag', 'hagyma', 'lilahagyma'),
(39, 1, 6, 39, '2016-05-31 00:00:00', 1, '160', 'csomag', 'hagyma', 'lilahagyma'),
(40, 1, 4, 40, '2016-05-31 00:00:00', 1, '170', 'csomag', 'hagyma', 'vöröshagyma'),
(41, 1, 5, 41, '2016-05-31 00:00:00', 1, '180', 'csomag', 'hagyma', 'vöröshagyma'),
(42, 1, 6, 42, '2016-05-31 00:00:00', 1, '165', 'csomag', 'hagyma', 'vöröshagyma'),
(43, 1, 4, 43, '2016-05-31 00:00:00', 1, '450', 'kiló', 'paradicsom', 'paradicsom'),
(44, 1, 5, 44, '2016-05-31 00:00:00', 1, '470', 'kiló', 'paradicsom', 'paradicsom'),
(45, 1, 6, 45, '2016-05-31 00:00:00', 1, '485', 'kiló', 'paradicsom', 'paradicsom'),
(46, 1, 9, 46, '2016-05-31 00:00:00', 1, '455', 'kiló', 'paradicsom', 'paradicsom'),
(47, 1, 4, 47, '2016-05-31 00:00:00', 1, '320', 'kiló', 'paprika', 'TV paprika'),
(48, 1, 9, 48, '2016-05-31 00:00:00', 1, '360', 'kiló', 'paprika', 'TV paprika'),
(49, 1, 7, 49, '2016-05-31 00:00:00', 1, '290', 'csomag', 'paprika', 'erős paprika'),
(50, 1, 4, 50, '2016-05-31 00:00:00', 1, '180', 'fej', 'saláta', 'fejes saláta'),
(51, 1, 5, 51, '2016-05-31 00:00:00', 1, '220', 'fej', 'saláta', 'fejes saláta'),
(52, 1, 6, 52, '2016-05-31 00:00:00', 1, '210', 'fej', 'saláta', 'fejes saláta'),
(53, 1, 4, 53, '2016-05-31 00:00:00', 1, '230', 'csomag', 'répa', 'sárgarépa'),
(54, 1, 5, 54, '2016-05-31 00:00:00', 1, '190', 'csomag', 'répa', 'sárgarépa'),
(55, 1, 6, 55, '2016-05-31 00:00:00', 1, '215', 'csomag', 'répa', 'sárgarépa'),
(56, 1, 4, 56, '2016-05-31 00:00:00', 1, '225', 'kiló', 'bab', 'zöldbab'),
(57, 1, 6, 57, '2016-05-31 00:00:00', 1, '45', 'fej', 'zeller', 'zeller'),
(58, 1, 9, 58, '2016-05-31 00:00:00', 1, '75', 'csomag', 'hagyma', 'póréhagyma'),
(59, 1, 6, 59, '2016-05-31 00:00:00', 1, '210', 'kiló', 'bab', 'tarka bab'),
(60, 2, 3, 60, '2016-05-31 00:00:00', 1, '440', 'kiló', 'málna', 'málna'),
(61, 4, 3, 61, '2016-05-31 00:00:00', 1, '370', 'csomag', 'aszalt gyümölcs', 'aszalt áfonya'),
(62, 2, 12, 62, '2016-05-31 00:00:00', 1, '410', 'kiló', 'málna', 'málna'),
(63, 1, 5, 63, '2016-05-31 00:00:00', 1, '100', 'csomag', 'hagyma', 'medvehagyma'),
(64, 1, 4, 64, '2016-05-31 00:00:00', 1, '55', 'csomag', 'petrezselyem', 'petrezselyem'),
(65, 1, 6, 65, '2016-05-31 00:00:00', 1, '50', 'csomag', 'petrezselyem', 'petrezselyem'),
(66, 3, 7, 66, '2016-05-31 00:00:00', 1, '350', 'liter', 'tej', 'tehéntej'),
(67, 3, 8, 67, '2016-05-31 00:00:00', 1, '345', 'liter', 'tej', 'tehéntej'),
(68, 3, 9, 68, '2016-05-31 00:00:00', 1, '360', 'liter', 'tej', 'tehéntej'),
(69, 3, 7, 69, '2016-05-31 00:00:00', 1, '420', 'liter', 'tej', 'kecsketej'),
(70, 3, 8, 70, '2016-05-31 00:00:00', 1, '400', 'liter', 'tej', 'kecsketej'),
(71, 3, 9, 71, '2016-05-31 00:00:00', 1, '405', 'liter', 'tej', 'kecsketej'),
(72, 3, 7, 72, '2016-05-31 00:00:00', 1, '390', 'kiló', 'sajt', 'sajt'),
(73, 3, 7, 73, '2016-05-31 00:00:00', 1, '525', 'kiló', 'sajt', 'kecskesajt'),
(74, 3, 8, 74, '2016-05-31 00:00:00', 1, '660', 'kiló', 'sajt', 'juhsajt'),
(75, 3, 9, 75, '2016-05-31 00:00:00', 1, '370', 'kiló', 'sajt', 'füstölt sajt'),
(76, 3, 8, 76, '2016-05-31 00:00:00', 1, '410', 'kiló', 'sajt', 'sajt'),
(77, 3, 9, 77, '2016-05-31 00:00:00', 1, '380', 'kiló', 'túró', 'túró'),
(78, 3, 8, 78, '2016-05-31 00:00:00', 1, '365', 'kiló', 'túró', 'túró'),
(79, 4, 12, 79, '2016-05-31 00:00:00', 1, '15', 'darab', 'tojás', 'tyúktojás'),
(80, 4, 6, 80, '2016-05-31 00:00:00', 1, '10', 'darab', 'tojás', 'tyúktojás'),
(81, 4, 10, 81, '2016-05-31 00:00:00', 1, '15', 'darab', 'tojás', 'tyúktojás'),
(82, 4, 9, 82, '2016-05-31 00:00:00', 1, '20', 'darab', 'tojás', 'tyúktojás'),
(83, 4, 3, 83, '2016-05-31 00:00:00', 1, '440', 'üveg', 'lekvár', 'málnalekvár'),
(84, 4, 12, 84, '2016-05-31 00:00:00', 1, '415', 'üveg', 'lekvár', 'meggylekvár'),
(85, 4, 11, 85, '2016-05-31 00:00:00', 1, '620', 'üveg', 'méz', 'akácméz'),
(86, 4, 11, 86, '2016-05-31 00:00:00', 1, '570', 'üveg', 'méz', 'virágméz'),
(87, 4, 11, 87, '2016-05-31 00:00:00', 1, '560', 'üveg', 'méz', 'repceméz'),
(88, 4, 10, 88, '2016-05-31 00:00:00', 1, '600', 'üveg', 'méz', 'virágméz'),
(89, 4, 12, 89, '2016-05-31 00:00:00', 1, '590', 'üveg', 'méz', 'virágméz'),
(90, 1, 9, 90, '2016-05-31 00:00:00', 1, '530', 'kiló', 'kukorica', 'csemege kukorica\r\n');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `product_categories`
--

CREATE TABLE `product_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `weight` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `product_categories`
--

INSERT INTO `product_categories` (`id`, `name`, `weight`) VALUES
(1, 'zöldség', 1),
(2, 'gyümölcs', 2),
(3, 'tejtermék', 3),
(4, 'kamra', 4);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `regions`
--

CREATE TABLE `regions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `regions`
--

INSERT INTO `regions` (`id`, `name`) VALUES
(1, 'Bács-Kiskun megye'),
(2, 'Baranya megye'),
(3, 'Békés megye'),
(4, 'Borsod-Abaúj-Zemplén megye'),
(5, 'Csongrád megye'),
(6, 'Fejér megye'),
(7, 'Győr-Moson-Sopron megye'),
(8, 'Hajdú-Bihar megye'),
(9, 'Heves megye'),
(10, 'Jász-Nagykun-Szolnok megye'),
(11, 'Komárom-Esztergom megye'),
(12, 'Nógrád megye'),
(13, 'Pest megye'),
(14, 'Somogy megye'),
(15, 'Szabolcs-Szatmár-Bereg megye'),
(16, 'Tolna megye'),
(17, 'Vas megye'),
(18, 'Veszprém megye'),
(19, 'Zala megye');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `reg_date` datetime NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `users`
--

INSERT INTO `users` (`id`, `reg_date`, `email`, `password`) VALUES
(1, '2016-07-06 00:00:00', 'mgerocs@gmail.com', '662c03c272ff006f12c9f16fc4992460c57585ee744bb093e0f7279ec7df5c01');

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- A tábla indexei `farms`
--
ALTER TABLE `farms`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_1DE06CC44E7AF8F` (`gallery_id`),
  ADD KEY `IDX_1DE06CC498260155` (`region_id`);

--
-- A tábla indexei `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_E01FBE6A4E7AF8F` (`gallery_id`);

--
-- A tábla indexei `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_B3BA5A5A4E7AF8F` (`gallery_id`),
  ADD KEY `IDX_B3BA5A5A12469DE2` (`category_id`),
  ADD KEY `IDX_B3BA5A5A65FCFA0D` (`farm_id`);

--
-- A tábla indexei `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `regions`
--
ALTER TABLE `regions`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `farms`
--
ALTER TABLE `farms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT a táblához `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;
--
-- AUTO_INCREMENT a táblához `images`
--
ALTER TABLE `images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;
--
-- AUTO_INCREMENT a táblához `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;
--
-- AUTO_INCREMENT a táblához `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT a táblához `regions`
--
ALTER TABLE `regions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT a táblához `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Megkötések a kiírt táblákhoz
--

--
-- Megkötések a táblához `farms`
--
ALTER TABLE `farms`
  ADD CONSTRAINT `FK_1DE06CC44E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `galleries` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_1DE06CC498260155` FOREIGN KEY (`region_id`) REFERENCES `regions` (`id`);

--
-- Megkötések a táblához `images`
--
ALTER TABLE `images`
  ADD CONSTRAINT `FK_E01FBE6A4E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `galleries` (`id`) ON DELETE CASCADE;

--
-- Megkötések a táblához `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `FK_B3BA5A5A4E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `galleries` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_B3BA5A5A65FCFA0D` FOREIGN KEY (`farm_id`) REFERENCES `farms` (`id`),
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `product_categories` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
