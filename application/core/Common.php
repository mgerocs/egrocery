<?php

class Common extends MY_Controller {

    // Doctrine EntityManager
    public $em;

    function __construct() {
        parent::__construct();
        $this->em = $this->doctrine->em;
        $is_logged_in = $this->session->userdata('is_logged_in');
        if (!isset($is_logged_in) || $is_logged_in != true) {
            $this->session->set_userdata('is_logged_in', false);
        }
    }

    function is_logged_in() {
        $is_logged_in = $this->session->userdata('is_logged_in');        
    }

    function getMaxWeight($entity) {
        try {
            $weight = 0;

            $repo = $this->em->getRepository($entity);
            $qb = $repo->createQueryBuilder('e');
            $qb->select('count(e.id)');

            if ($qb->getQuery()->getSingleScalarResult() > 0) {
                $qb->select('MAX(e.weight)');
                $max = $qb->getQuery()->getSingleScalarResult();
                $weight = $max + 1;
            } else {
                $weight = 1;
            }

            return $weight;
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function reorderEntity($data, $entity) {
        try {
            $ids = $data['ids'];
            $weights = $data['weights'];

            if (empty($ids) || empty($weights)) {
                throw new Exception("No data posted.");
            }

            for ($i = 0; $i < count($ids); $i++) {
                $item = $this->em->getRepository($entity)->find($ids[$i]);
                $item->setWeight($weights[$i]);
            }
            $this->em->flush();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function validation($ruleset) {
        if ($this->form_validation->run($ruleset) == FALSE) {
            $status = false;
        } else {
            $status = true;
        }
        return $status;
    }

    function sendErrorMessage($message) {
        echo json_encode(array('status' => false, 'error' => $message));
        exit();
    }

}
