<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="products")
 */
class Product {

    /**
     * @Id
     * @Column(type="integer", nullable=false, options={"unsigned":true})
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Column(type="datetime", nullable=false)
     */
    protected $create_date;

    /**
     * @ManyToOne(targetEntity="ProductCategory", inversedBy="product")
     * @JoinColumn(name="category_id", referencedColumnName="id", nullable=false)
     */
    protected $category;
    
    /**
     * @ManyToOne(targetEntity="Farm", inversedBy="product")
     * @JoinColumn(name="farm_id", referencedColumnName="id")
     */
    protected $farm;

    /**
     * @Column(type="boolean")
     */
    protected $status;

    /**
     * @Column(type="decimal", nullable=false, options={"unsigned":true})
     */
    protected $price;

    /**
     * @Column(type="string", nullable=false)
     */
    protected $measure;

    /**
     * @Column(type="string", nullable=false, length=50)
     */
    protected $label;

    /**
     * @Column(type="string", nullable=false, length=100)
     */
    protected $name;

    /**
     * @OneToOne(targetEntity="Gallery", cascade={"remove"})
     * @JoinColumn(name="gallery_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $gallery;

    public function __construct() {
        $this->status = 1;
        $this->create_date = new \DateTime("now");
    }

    function getId() {
        return $this->id;
    }

    function getCreate_date() {
        return $this->create_date;
    }

    function getCategory() {
        return $this->category;
    }
    
    function getFarm() {
        return $this->farm;
    }

    function getStatus() {
        return $this->status;
    }

    function getPrice() {
        return $this->price;
    }

    function getMeasure() {
        return $this->measure;
    }

    function getLabel() {
        return $this->label;
    }

    function getName() {
        return $this->name;
    }

    function getImages() {
        return $this->images;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setCreate_date($create_date) {
        $this->create_date = $create_date;
    }

    function setCategory($category) {
        $this->category = $category;
    }
    
    function setFarm($farm) {
        $this->farm = $farm;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function setPrice($price) {
        $this->price = $price;
    }

    function setMeasure($measure) {
        $this->measure = $measure;
    }

    function setLabel($label) {
        $this->label = $label;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setImages($images) {
        $this->images = $images;
    }

}
