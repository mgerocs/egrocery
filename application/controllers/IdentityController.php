<?php

class IdentityController extends Common {

    function __construct() {
        parent::__construct();
    }

    function checkIfLoggedIn() {
        echo json_encode(array(
            'is_logged_in' => $this->session->userdata('is_logged_in'),
            'email' => $this->session->userdata('email')
        ));
        exit();
    }

    function checkCredentials() {

        $status = true;
        $error = "";

        $params = json_decode(file_get_contents('php://input'), true);

        $repo = $this->em->getRepository('Entity\User');
        $qb = $repo->createQueryBuilder('u');
        $qb->select('u.id', 'u.email');
        $qb->where('u.email = :email');
        $qb->andWhere('u.password = :password');
        $qb->setParameter('email', $params['email']);
        $qb->setParameter('password', hash('sha256', $params['password']));

        $user = $qb->getQuery()->getOneOrNullResult();

        if (empty($user)) {
            $error = 'Wrong username or password.';            
            $isLoggedIn = false;
        } else {
            $this->session->set_userdata('is_logged_in', true);
            $this->session->set_userdata('email', $params['email']);
            $isLoggedIn = true;
        }
        echo json_encode(array('status' => $status, 'email' => $params['email'], 'is_logged_in' => $isLoggedIn, 'error' => $error));
        exit();
    }

    function logout() {
        $this->session->sess_destroy();
        echo json_encode(array('status' => true, 'is_logged_in' => false));
        exit();
    }

}
