(function () {
    'use strinct';

    var GroceryApp = angular.module('GroceryApp');

    /***************************************************************************
     CONSTANTS
     ***************************************************************************/
    GroceryApp.constant('BaseUrl', base_url);

    /***************************************************************************
     ROUTING
     ***************************************************************************/
    GroceryApp.config(['$routeProvider',
        function ($routeProvider) {
            $routeProvider.
                    when('/', {
                        templateUrl: 'src/app/templates/home.html',
                        controller: 'HomeController',
                        controllerAs: 'home'
                    }).
                    when('/home', {
                        templateUrl: 'src/app/templates/home.html',
                        controller: 'HomeController',
                        controllerAs: 'home'
                    }).
                    when('/products', {
                        templateUrl: 'src/app/templates/products.html',
                        controller: 'ProductController',
                        controllerAs: 'product'
                    }).
                    when('/product/:id', {
                        templateUrl: 'src/app/templates/product.html',
                        controller: 'ProductController',
                        controllerAs: 'product'
                    }).
                    when('/farms', {
                        templateUrl: 'src/app/templates/farms.html',
                        controller: 'FarmController',
                        controllerAs: 'farm'
                    }).
                    when('/farm/:id', {
                        templateUrl: 'src/app/templates/farm.html',
                        controller: 'FarmController',
                        controllerAs: 'farm'
                    }).
                    when('/overview', {
                        templateUrl: 'src/app/templates/overview.html',
                        controller: 'CartController',
                        controllerAs: 'cart'
                    }).
                    when('/delivery', {
                        templateUrl: 'src/app/templates/delivery.html',
                        controller: 'DeliveryController',
                        controllerAs: 'delivery'
                    }).
                    when('/payment', {
                        templateUrl: 'src/app/templates/payment.html',
                        controller: 'OrderController',
                        controllerAs: 'order'
                    }).
                    when('/about', {
                        templateUrl: 'src/app/templates/about.html'                        
                    }).
                    otherwise({
                        redirectTo: '/home'
                    });
        }]);

    /***************************************************************************
     RUN
     ***************************************************************************/
    GroceryApp.run(function ($sessionStorage, User, Cart, ProductCategory, ProductFilter, Region, Farm) {

        //pass preloaded data to services
        User.setUser(userdata);
        ProductCategory.setCategories(categories);
        Region.setRegions(regions);

        //initialize cart
        if (!$sessionStorage.cart) {
            if (Cart.getCartId() === null) {
                Cart.initCart();
            }
            $sessionStorage.cart = Cart.getCart();
        } else {
            Cart.setCart($sessionStorage.cart);
        }

        //initialize product filter
        if (!$sessionStorage.filter) {
            $sessionStorage.filter = ProductFilter.getFilter();
        } else {
            ProductFilter.setFilter($sessionStorage.filter);
        }

    });

})();