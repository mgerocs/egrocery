(function () {

    var userdata;
    var categories;
    var regions;

    var GroceryApp = angular.module('GroceryApp', ['ngRoute', 'ngStorage', 'ui.bootstrap']);

    function fetchData() {
        var initInjector = angular.injector(["ng"]);
        var $http = initInjector.get("$http");
        var $q = initInjector.get("$q");
        
        var spinner = document.createElement("img");
        spinner.src = "spinner.gif";
        spinner.id = "spinner";
        document.body.appendChild(spinner);

        var request0 = $http.get(base_url + "identity/checkIfLoggedIn");
        var request1 = $http.get(base_url + "grocery/getProductCategories");
        var request2 = $http.get(base_url + "grocery/getRegions");

        return $q.all([request0, request1, request2]).then(function (results) {
            userdata = results[0].data;
            categories = results[1].data.items;
            regions = results[2].data.items;
        })
                .catch(function (error) {
                    console.log("Error:", error);
                })
                .finally(function () {
                    spinner.remove();
                });
    }

    function bootstrapApplication(userdata, categories, regions) {
        angular.element(document).ready(function () {
            angular.bootstrap(document, ["GroceryApp"]);
        });
    }

    /***************************************************************************
     BOOTSTRAP APPLICATION
     ***************************************************************************/
    fetchData().then(function () {
        bootstrapApplication(userdata, categories, regions);
    });


    /***************************************************************************
     CONSTANTS
     ***************************************************************************/
    GroceryApp.constant('BaseUrl', base_url);
    /***************************************************************************
     ROUTING
     ***************************************************************************/
    GroceryApp.config(['$routeProvider',
        function ($routeProvider) {
            $routeProvider.
                    when('/', {
                        templateUrl: 'templates/home.html',
                        controller: 'HomeController',
                        controllerAs: 'home'

                    }).
                    when('/home', {
                        templateUrl: 'templates/home.html',
                        controller: 'HomeController',
                        controllerAs: 'home'

                    }).
                    when('/products', {
                        templateUrl: 'templates/products.html',
                        controller: 'ProductController',
                        controllerAs: 'product'
                    }).
                    when('/product/:id', {
                        templateUrl: 'templates/product.html',
                        controller: 'ProductController',
                        controllerAs: 'product'
                    }).
                    when('/farms', {
                        templateUrl: 'templates/farms.html',
                        controller: 'FarmController',
                        controllerAs: 'farm'
                    }).
                    when('/farm/:id', {
                        templateUrl: 'templates/farm.html',
                        controller: 'FarmController',
                        controllerAs: 'farm'
                    }).
                    when('/overview', {
                        templateUrl: 'templates/overview.html',
                        controller: 'CartController',
                        controllerAs: 'cart'
                    }).
                    when('/delivery', {
                        templateUrl: 'templates/delivery.html',
                        controller: 'OrderController',
                        controllerAs: 'order'
                    }).
                    when('/payment', {
                        templateUrl: 'templates/payment.html',
                        controller: 'OrderController',
                        controllerAs: 'order'
                    }).
                    otherwise({
                        redirectTo: '/home'
                    });
        }]);

    /***************************************************************************
     RUN
     ***************************************************************************/
    GroceryApp.run(function ($sessionStorage, User, Cart, ProductCategory, ProductFilter, Region, Farm) {

        //pass preloaded data to services
        User.setUser(userdata);
        ProductCategory.setCategories(categories);
        Region.setRegions(regions);

        //initialize cart
        if (!$sessionStorage.cart) {
            if (Cart.getCartId() === null) {
                Cart.initCart();
            }
            $sessionStorage.cart = Cart.getCart();
        } else {
            Cart.setCart($sessionStorage.cart);
        }

        //initialize product filter
        if (!$sessionStorage.filter) {
            $sessionStorage.filter = ProductFilter.getFilter();
        } else {
            ProductFilter.setFilter($sessionStorage.filter);
        }

    });

    /***************************************************************************
     ROOT CONTROLLER
     ***************************************************************************/
    GroceryApp.controller('RootController', RootController);
    RootController.$inject = ['$scope', 'User', 'Cart', 'ProductCategory', 'Region', 'Navigation', 'BaseUrl'];
    function RootController($scope, User, Cart, ProductCategory, Region, Navigation, BaseUrl) {
        var root = this;
        root.base = BaseUrl;
        root.currentMenuItem = Navigation.getCurrentMenuItem();

        root.isLoggedIn = User.checkIfLoggedIn();
        root.user = User.getEmail();

        root.categories = ProductCategory.getCategories();
        root.regions = Region.getRegions();

        root.cart = Cart.getCart();

        $scope.$watch(
                function () {
                    return User.getUser();
                },
                function (newValue, oldValue) {
                    root.isLoggedIn = newValue.is_logged_in;
                    root.user = newValue.email;
                }, true
                );

        $scope.$watch(
                function () {
                    return Cart.getCart();
                },
                function (newValue, oldValue) {
                    root.cart = newValue;
                }, true
                );

        $scope.$watch(
                function () {
                    return Navigation.getCurrentMenuItem();
                },
                function (newValue, oldValue) {
                    root.currentMenuItem = newValue;
                }, true
                );

        root.focussed = false;

        root.pageClass = "default-page";


//        root.getClass = function (path) {
//            return ($location.path().substr(0, path.length) === path) ? 'active' : '';
//        };
    }
    ;

    /***************************************************************************
     ***** IDENTITY CONTROLLER
     ***************************************************************************/
    GroceryApp.controller('IdentityController', IdentityController);
    IdentityController.$inject = ['$rootScope', '$scope', 'User'];
    function IdentityController($rootScope, $scope, User) {
        var identity = this;
        identity.error = "";
        identity.email = "";
        identity.password = "";
        $scope.focussed = false;

        identity.dropDown = function () {
            $scope.focussed = true;
        };

        identity.login = function () {
            $rootScope.$broadcast("loading", {on: true});
            var promise = User.login(identity.email, identity.password);
            promise.
                    then(function (response) {
                        identity.error = response.data.error;
                        identity.email = "";
                        identity.password = "";
                        $scope.loginform.$setPristine();
                        $scope.loginform.$setValidity();
                        User.setIfLoggedIn(response.data.is_logged_in);
                        User.setEmail(response.data.email);
                    })
                    .catch(function (error) {
                        console.log("Error:", error);
                    })
                    .finally(function () {
                        $rootScope.$broadcast("loading", {on: false});
                    });

        };

        identity.logout = function () {
            $rootScope.$broadcast("loading", {on: true});
            var promise = User.logout();
            promise.
                    then(function (response) {
                        identity.error = "";
                        identity.email = "";
                        identity.password = "";
                        $scope.loginform.$setPristine();
                        $scope.loginform.$setValidity();
                        User.setIfLoggedIn(false);
                        User.setEmail("");
                    })
                    .catch(function (error) {
                        console.log("Error:", error);
                    })
                    .finally(function () {
                        $rootScope.$broadcast("loading", {on: false});
                    });
        };

        identity.innerClick = function ($event) {
            var id = $event.target.id;
            if (id === "paybutton" || id === "regbutton") {
                return true;
            }
            $event.stopPropagation();
            return false;
        };
    }

    /***************************************************************************
     ***** HOME CONTROLLER
     ***************************************************************************/
    GroceryApp.controller('HomeController', HomeController);
    HomeController.$inject = ['$location', 'ProductFilter', 'Navigation'];
    function HomeController($location, ProductFilter, Navigation) {
        var home = this;
        Navigation.setCurrentMenuItem("");
        home.selectCategory = function (category_id) {
            ProductFilter.reset();
            ProductFilter.setFilterCategory(category_id);
            $location.path("/products");
        };
    }

    /***************************************************************************
     ***** NAV CONTROLLER
     ***************************************************************************/
    GroceryApp.controller('NavController', NavController);
    NavController.$inject = ['Navigation'];
    function NavController(Navigation) {
        var nav = this;
        nav.setCurrentMenuItem = function (menuitem) {
            Navigation.setCurrentMenuItem(menuitem);
        };
    }

    /***************************************************************************
     ***** PRODUCT CONTROLLER
     ***************************************************************************/
    GroceryApp.controller('ProductController', ProductController);
    ProductController.$inject = ['$scope', '$rootScope', '$route', '$routeParams', '$q', '$location', '$sessionStorage', 'ProductCategory', 'Product', 'ProductFilter', 'Farm', 'Cart', 'Navigation'];
    function ProductController($scope, $rootScope, $route, $routeParams, $q, $location, $sessionStorage, ProductCategory, Product, ProductFilter, Farm, Cart, Navigation) {

        var product = this;
        Navigation.setCurrentMenuItem("products");

        //if categories are not set redirect home
        if (!$sessionStorage.categories) {
            $location.path("/home");
        } else {
            ProductCategory.setCategories($sessionStorage.categories);
        }
        //default to "zöldség"
        if (!ProductFilter.getFilter().category_id) {
            ProductFilter.setFilterCategory(1);
        }

        product.title = 'Termékek';
        //$rootScope.currentNavItem = 'products';

        product.category_id = ProductFilter.getFilter().category_id;
        product.category_name = ProductCategory.getCategoryName(product.category_id);

        if (!$routeParams.id) {
            ProductFilter.setFilterPId(null);
        } else {
            ProductFilter.setFilterPId($routeParams.id);
        }

        $rootScope.$broadcast("loading", {on: true});

        var promise0 = Product.getProductLabels(product.category_id);
        var promise1 = Product.getProducts(ProductFilter.getFilter());
        var promise2 = Farm.getFarms();

        $q.all([promise0, promise1, promise2])
                .then(function (response) {
                    product.labels = (response[0].data.items || []);
                    product.products = (response[1].data.items || []);
                    product.farms = (response[2].data.items || []);
                })
                .catch(function (error) {
                    console.log("Error:", error);
                })
                .finally(function () {
                    $rootScope.$broadcast("loading", {on: false});
                });

        //initialize product filters
        product.labelFilter = (ProductFilter.getFilter().label || "");
        product.farmFilter = (ProductFilter.getFilter().farm_id || "");
        product.regionFilter = (ProductFilter.getFilter().region_id || "");

        //category filter
        product.selectCategory = function (category_id) {
            ProductFilter.reset();
            ProductFilter.setFilterCategory(category_id);
            product.category_id = ProductFilter.getFilter().category_id;
            product.category_name = ProductCategory.getCategoryName(product.category_id);
            $route.reload();
        };
        //update product filters
        product.setLabelFilter = function (label) {
            ProductFilter.setFilterLabel(label);
            product.labelFilter = label;
        };
        product.setRegionFilter = function () {
            ProductFilter.setFilterRegion(product.regionFilter);
        };
        product.setFarmFilter = function () {
            ProductFilter.setFilterFarm(product.farmFilter);
        };

//        product.setProductFilter = function (category_id, region_id, farm_id, label) {
//            ProductFilter.setFilterCategory(category_id);
//            ProductFilter.setFilterRegion(region_id);
//            ProductFilter.setFilterFarm(farm_id);
//            ProductFilter.setFilterLabel(label);
//            $location.path("/products");
//        };

        //add to cart        
        product.addToCart = function (item) {
            if (item && item.amount !== 0) {
                Cart.addToCart(item);
            }
            $scope.count = 1;
            //cart button animation
            var $cartButton = angular.element(document.querySelector('#cart-button'));
            $cartButton.addClass('added_item');
            setTimeout(function () {
                $cartButton.removeClass('added_item');
            }, 500);
        };
        //check if in cart
        product.checkIfInCart = function (product) {
            return Cart.checkIfInCart(product);
        };
    }

    /***************************************************************************
     ***** FARM CONTROLLER
     ***************************************************************************/
    GroceryApp.controller('FarmController', FarmController);
    FarmController.$inject = ['$rootScope', '$routeParams', '$location', '$timeout', '$sessionStorage', 'Farm', 'ProductCategory', 'ProductFilter', 'Navigation'];
    function FarmController($rootScope, $routeParams, $location, $timeout, $sessionStorage, Farm, ProductCategory, ProductFilter, Navigation) {
        var farm = this;
        Navigation.setCurrentMenuItem("farms");

        if (!$sessionStorage.categories) {
            $location.path("/home");
        } else {
            ProductCategory.setCategories($sessionStorage.categories);
        }

        farm.title = 'Gazdaságok';

        $rootScope.$broadcast("loading", {on: true});

        var promise = {};
        if ($routeParams.id) {
            promise = Farm.getFarm($routeParams.id);
        } else {
            promise = Farm.getFarms();
        }

        promise
                .then(function (response) {
                    farm.farms = (response.data.items || []);
                    $timeout(function () {
                        var $map = document.getElementById('map');
                        if ($map) {
                            Farm.showOnMap(farm.farms[0].lat, farm.farms[0].lon);
                        }
                    });
                })
                .catch(function (error) {
                    console.log("Error:", error);
                })
                .finally(function () {
                    $rootScope.$broadcast("loading", {on: false});
                });

        farm.Math = window.Math;
        farm.sortVal = 'name';
        farm.sortDir = false;

        farm.change = function () {
            if (farm.sortVal === "name") {
                farm.sortVal = "rating";
                farm.sortDir = true;
            } else {
                farm.sortVal = "name";
                farm.sortDir = false;
            }
        };

        farm.setProductFilter = function (category_id, region_id, farm_id, label) {
            ProductFilter.setFilterCategory(category_id);
            ProductFilter.setFilterRegion(region_id);
            ProductFilter.setFilterFarm(farm_id);
            ProductFilter.setFilterLabel(label);
            $location.path("/products");
        };

    }

    /***************************************************************************
     ***** CART CONTROLLER
     ***************************************************************************/
    GroceryApp.controller('CartController', CartController);
    CartController.$inject = ['$scope', 'Cart', 'Navigation'];
    function CartController($scope, Cart, Navigation) {
        var cart = this;
        Navigation.setCurrentMenuItem("");
        cart.modifyCart = function (id, amount) {
            Cart.modifyCart(id, amount);
        };
        cart.removeFromCart = function (id) {
            Cart.removeFromCart(id);
        };
//        cart.setProductFilter = function (category_id, region_id, farm_id, label) {
//            ProductFilter.setFilterCategory(category_id);
//            ProductFilter.setFilterRegion(region_id);
//            ProductFilter.setFilterFarm(farm_id);
//            ProductFilter.setFilterLabel(label);
//            $location.path("/products");
//        };

    }
//    GroceryApp.controller('OrderController', function ($rootScope, $scope, $http) {
//
//    });

    /***************************************************************************
     ***** SPINNER CONTROLLER
     ***************************************************************************/
    GroceryApp.controller('SpinnerController', SpinnerController);
    SpinnerController.$inject = ['$rootScope', '$scope'];
    function SpinnerController($rootScope, $scope) {
        var spinner = this;
        spinner.showSpinner = false;
        //returns a cancellation function
        var deregister = $rootScope.$on('loading', function (event, data) {
            spinner.showSpinner = data.on;
        });
        $scope.$on('$destroy', deregister);
    }
    ;

    //*** USER SERVICE ***//
    GroceryApp.service('User', User);
    User.$inject = ['$http', 'BaseUrl'];
    function User($http, BaseUrl) {
        var service = this;
        var _isLoggedIn = false;
        var _email = "";
        service.checkIfLoggedIn = function () {
            return _isLoggedIn;
        };
        service.setIfLoggedIn = function (isLoggedIn) {
            _isLoggedIn = isLoggedIn;
        };
        service.getEmail = function () {
            return _email;
        };
        service.setEmail = function (email) {
            _email = email;
        };
        service.login = function (email, password) {
            var response = $http({
                method: "POST",
                url: (BaseUrl + "identity/checkCredentials"),
                data: {
                    'email': email,
                    'password': password
                },
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });
            return response;
        };
        service.logout = function () {
            var response = $http({
                method: "GET",
                url: (BaseUrl + "identity/logout")
            });
            return response;
        };
        service.getUser = function () {
            return {
                is_logged_in: _isLoggedIn,
                email: _email
            };
        };
        service.setUser = function (userdata) {
            _isLoggedIn = userdata.is_logged_in;
            _email = (userdata.email || "");
        };
    }
    ;

    //*** PRODUCT CATEGORY SERVICE ***//
    GroceryApp.service('ProductCategory', ProductCategory);
    ProductCategory.$inject = ['$sessionStorage'];
    function ProductCategory($sessionStorage) {
        var service = this;
        var _categories = [];
        service.getCategoryName = function (category_id) {
            return _categories[category_id].name;
        };
        service.getCategories = function () {
            return _categories;
        };
        service.setCategories = function (categories) {
            _categories = categories;
            $sessionStorage.categories = this.getCategories();
        };
        return service;
    }
    ;

    //*** PRODUCT SERVICE ***//
    GroceryApp.service('Product', Product);
    Product.$inject = ['$http', 'BaseUrl'];
    function Product($http, BaseUrl) {
        var service = this;
        service.getProductLabels = function (category_id) {
            var response = $http({
                method: "GET",
                url: (BaseUrl + "grocery/getProductLabels/" + category_id)
            });
            return response;
        };
        service.getProducts = function (filter) {
            var response = $http({
                method: "POST",
                url: (BaseUrl + "grocery/getProducts"),
                data: {'filter': filter},
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });
            return response;
        };
    }
    ;

    //*** REGION SERVICE ***//
    GroceryApp.service('Region', Region);
    function Region() {
        var service = this;
        var _regions = [];
        service.getRegions = function () {
            return _regions;
        };
        service.setRegions = function (regions) {
            _regions = regions;
        };
        return service;
    }
    ;
    //*** FARM SERVICE ***//
    GroceryApp.service('Farm', Farm);
    Farm.$inject = ['$http', 'BaseUrl'];
    function Farm($http, BaseUrl) {
        var service = this;
        service.getFarms = function () {
            var response = $http({
                method: "GET",
                url: (BaseUrl + "grocery/getFarms")
            });
            return response;
        };
        service.getFarm = function (farm_id) {
            var response = $http({
                method: "GET",
                url: (BaseUrl + "grocery/getFarms/" + farm_id)
            });
            return response;
        };
        service.showOnMap = function (lat, lon) {
            var target = new google.maps.LatLng(lat, lon);
            var center = new google.maps.LatLng(47.162494, 19.503304);
            var marker;
            var map;

            var mapOptions = {
                zoom: 6,
                center: center,
                disableDefaultUI: true
            };

            map = new google.maps.Map(document.getElementById('map'), mapOptions);

            marker = new google.maps.Marker({
                map: map,
                draggable: false,
                animation: google.maps.Animation.DROP,
                position: target
            });
        };
    }
    ;
    //*** PRODUCT FILTER SERVICE ***//
    GroceryApp.service('ProductFilter', ProductFilter);
    ProductFilter.$inject = ['$sessionStorage'];
    function ProductFilter($sessionStorage) {
        var service = this;
        var _filter = {};
        service.getFilter = function () {
            return _filter;
        };
        service.setFilterCategory = function (category_id) {
            _filter.category_id = category_id;
            $sessionStorage.filter = this.getFilter();
        };
        service.setFilterRegion = function (region_id) {
            _filter.region_id = region_id;
            $sessionStorage.filter = this.getFilter();
        };
        service.setFilterFarm = function (farm_id) {
            _filter.farm_id = farm_id;
            $sessionStorage.filter = this.getFilter();
        };
        service.setFilterLabel = function (label) {
            _filter.label = label;
            $sessionStorage.filter = this.getFilter();
        };
        service.setFilterPId = function (product_id) {
            _filter.product_id = product_id;
            $sessionStorage.filter = this.getFilter();
        };
        service.setFilter = function (filter) {
            _filter = filter;
            $sessionStorage.filter = this.getFilter();
        };
        service.reset = function () {
            // _filter.category_id = null;
            _filter.region_id = null;
            _filter.farm_id = null;
            _filter.label = null;
            _filter.product_id = null;
            $sessionStorage.filter = this.getFilter();
        };
        return service;
    }
    ;

    //*** CART SERVICE ***//
    GroceryApp.service('Cart', Cart);
    Cart.$inject = ['$sessionStorage'];
    function Cart($sessionStorage) {
        var _cart_id = null;
        var _cart_date;
        var _items = {};
        var _pcs;
        var _total;
        this.initCart = function () {
            _cart_id = this.generateId(8);
            _cart_date = new Date();
            _items = {};
            _pcs = 0;
            _total = 0;
        };
        this.getCartId = function () {
            return _cart_id;
        };
        this.addToCart = function (item) {
            var product = item.product;
            var amount = item.amount;
            if (!this.checkIfInCart(product)) {
                var newitem = {
                    'id': product.id,
                    'category_id': product.category_id,
                    'label': product.label,
                    'name': product.name,
                    'farm_id': product.farm_id,
                    'region_id': product.region_id,
                    'farmname': product.farmname,
                    'price': product.price,
                    'amount': amount
                };
                _items[product.id] = newitem;
            } else {
                _items[product.id].amount += amount;
            }

            this.calculatePrice();
            $sessionStorage.cart = this.getCart();
            return {
                'products': _items,
                'total': _total
            };
        };
        this.calculatePrice = function () {
            var sum = 0;
            var pcs = 0;
            for (var key in _items) {
                sum += _items[key].amount * _items[key].price;
                pcs += 1;
            }
            _total = sum;
            _pcs = pcs;
        };
        this.generateId = function (len) {
            var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
            var string_length = len;
            var randomstring = '';
            for (var x = 0; x < string_length; x++) {

                var letterOrNumber = Math.floor(Math.random() * 2);
                if (letterOrNumber === 0) {
                    var newNum = Math.floor(Math.random() * 9);
                    randomstring += newNum;
                } else {
                    var rnum = Math.floor(Math.random() * chars.length);
                    randomstring += chars.substring(rnum, rnum + 1);
                }
            }
            return randomstring;
        };
        this.checkIfInCart = function (product) {
            return product.id in _items;
        };
        this.getCart = function () {
            return {
                'cart_id': _cart_id,
                'cart_date': _cart_date,
                'products': _items,
                'pcs': _pcs,
                'total': _total
            };
        };
        this.setCart = function (cart) {
            _cart_id = cart.id;
            _cart_date = cart.cart_date;
            for (var p in cart.products) {
                _items[p] = cart.products[p];
            }
            _pcs = cart.pcs;
            _total = cart.total;
        };
        this.emptyCart = function () {
            _items = {};
            _total = 0;
            $sessionStorage.cart = this.getCart();
        };
        this.modifyCart = function (id, amount) {
            _items[id].amount += amount;
            this.calculatePrice();
            $sessionStorage.cart = this.getCart();

        };
        this.removeFromCart = function (id) {
            delete _items[id];
            this.calculatePrice();
            $sessionStorage.cart = this.getCart();
        };

    }
    ;

    //*** NAVIGATION SERVICE ***//
    GroceryApp.service('Navigation', Navigation);
    function Navigation() {
        var service = this;
        var _currentMenuItem = "";
        service.getCurrentMenuItem = function () {
            return _currentMenuItem;
        };
        service.setCurrentMenuItem = function (menuitem) {
            _currentMenuItem = menuitem;
        };
        return service;
    }
    ;

    //*** NAVIGATION DIRECTIVE ***//
    GroceryApp.directive('menuItems', MenuItems);
    function MenuItems() {
        //directive definition object
        var ddo = {
            restrict: 'E',
            templateUrl: 'templates/navigation.html',
            controller: 'NavController',
            controllerAs: 'nav'
        };
        return ddo;
    }

    //*** IDENTITY DIRECTIVE ***//
    GroceryApp.directive('userPanel', UserPanel);
    function UserPanel() {
        var ddo = {
            restrict: 'E',
            templateUrl: 'templates/login.html',
            controller: 'IdentityController',
            controllerAs: 'identity'
        };
        return ddo;
    }

    //*** CART DIRECTIVE ***//
    GroceryApp.directive('cartPanel', CartPanel);
    function CartPanel() {
        var ddo = {
            restrict: 'E',
            templateUrl: 'templates/cart.html',
            controller: 'RootController',
            controllerAs: 'root'
        };
        return ddo;
    }

    //*** PRODUCT LIST ITEM DIRECTIVE ***//
    GroceryApp.directive('productListItem', ProductListItem);
    function ProductListItem() {
        var ddo = {
            restrict: 'E',
            templateUrl: 'templates/product-list-item.html'
        };
        return ddo;
    }

    //*** FARM RATING DIRECTIVE ***//
    GroceryApp.directive('farmRating', FarmRating);
    function FarmRating() {
        var ddo = {
            restrict: 'E',
            templateUrl: 'templates/farm-rating.html'
        };
        return ddo;
    }

    GroceryApp.directive('back', function ($window) {
        return {
            restrict: 'A',
            link: function (scope, elem, attrs) {
                elem.bind('click', function () {
                    $window.history.back();
                });
            }
        };
    });
    GroceryApp.directive('focusme', function ($timeout, $parse) {
        return {
            link: function (scope, elem, attrs) {
                var model = $parse(attrs.focusme);
                scope.$watch(model, function (value) {
                    if (value === true) {
                        $timeout(function () {
                            elem[0].focus();
                        }, 100);
                    }
                });
                elem.bind('blur', function () {
                    scope.$apply(function () {
                        scope.focussed = false;
                    });
                });
            }
        };
    });
    GroceryApp.directive("offCanvasMenu", function () {
        return {
            restrict: 'A',
            replace: false,
            link: function (scope, element) {
                scope.isMenuOpen = false;
                scope.toggleMenu = function () {
                    scope.isMenuOpen = !scope.isMenuOpen;
                };
                element.bind('click', function (e) {
                    if (e.target.tagName.toLowerCase() === 'a') {
                        scope.isMenuOpen = false;
                    }
                });
            }
        };
    });

    //*** SPINNER DIRECTIVE ***//
    GroceryApp.directive('spinner', Spinner);
    function Spinner() {
        var ddo = {
            restrict: 'E',
            templateUrl: 'templates/spinner.html',
            controller: 'SpinnerController',
            controllerAs: 'spinner'
        };
        return ddo;
    }

    GroceryApp.filter('range', function () {
        return function (input, total) {
            total = parseInt(total);
            for (var i = 0; i < total; i++) {
                input.push(i);
            }
            return input;
        };
    });
    GroceryApp.filter("toArray", function () {
        return function (obj) {
            var result = [];
            angular.forEach(obj, function (val, key) {
                result.push(val);
            });
            return result;
        };
    });
}());