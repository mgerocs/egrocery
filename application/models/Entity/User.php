<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="users")
 */
class User {

    /**
     * @Id
     * @Column(type="integer", nullable=false, options={"unsigned":true})
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Column(type="datetime", nullable=false)
     */
    protected $reg_date;

    /**
     * @Column(type="string", nullable=false, length=100)
     */
    protected $email;

    /**
     * @Column(type="string", nullable=false, length=64)     
     */
    protected $password;

    public function __construct() {
        $this->reg_date = new \DateTime("now");
    }

    function getId() {
        return $this->id;
    }

    function getReg_date() {
        return $this->reg_date;
    }

    function getEmail() {
        return $this->email;
    }

    function getPassword() {
        return $this->password;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setReg_date($reg_date) {
        $this->reg_date = $reg_date;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setPassword($password) {
        $this->password = $password;
    }

}
