<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="farms")
 */
class Farm {

    /**
     * @Id
     * @Column(type="integer", nullable=false, options={"unsigned":true})
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Column(type="boolean")
     */
    protected $status;

    /**
     * @Column(type="datetime", nullable=false)
     */
    protected $create_date;

    /**
     * @Column(type="string", nullable=false, length=100)
     */
    protected $name;
    
    /**
     * @Column(type="string", nullable=true, length=100)
     */
    protected $location;

    /**
     * @Column(type="decimal", nullable=true)
     */
    protected $lat;

    /**
     * @Column(type="decimal", nullable=true)
     */
    protected $lon; 
    
    /**
     * @Column(type="string", nullable=false, length=100)
     */
    protected $email;
    
    /**
     * @Column(type="string", nullable=true, length=3000)
     */
    protected $description;
        
    /**
     * @Column(type="integer", nullable=false, options={"unsigned":true})
     */
    protected $ratings_number;   
    
    /**
     * @Column(type="decimal", nullable=false, options={"unsigned":true})
     */ 
    protected $rating;

    /**
     * @OneToMany(targetEntity="Product", mappedBy="farm", cascade={"remove"})     
     */
    protected $product;

    /**
     * @OneToOne(targetEntity="Gallery", cascade={"remove"})
     * @JoinColumn(name="gallery_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $gallery;

    /**
     * @ManyToOne(targetEntity="Region", inversedBy="farm")
     * @JoinColumn(name="region_id", referencedColumnName="id")
     */
    protected $region;

    public function __construct() {
        $this->status = 1;
        $this->create_date = new \DateTime("now");
        $this->ratings_number = 0;
        $this->rating = 0;
    }

    function getId() {
        return $this->id;
    }

    function getStatus() {
        return $this->status;
    }

    function getCreate_date() {
        return $this->create_date;
    }

    function getName() {
        return $this->name;
    }
    
    function getLocation() {
        return $this->location;
    }

    function getEmail() {
        return $this->email;
    }

    function getDescription() {
        return $this->description;
    }

    function getLat() {
        return $this->lat;
    }

    function getLon() {
        return $this->lon;
    }
    
    function getRatings_number() {
        return $this->ratings_number;
    }

    function getRating() {
        return $this->rating;
    }    

    function getProduct() {
        return $this->product;
    }

    function getGallery() {
        return $this->gallery;
    }

    function getRegion() {
        return $this->region;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function setCreate_date($create_date) {
        $this->create_date = $create_date;
    }

    function setName($name) {
        $this->name = $name;
    }
    
    function setLocation($location) {
        $this->location = $location;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setLat($lat) {
        $this->lat = $lat;
    }

    function setLon($lon) {
        $this->lon = $lon;
    }
    
    function setRatings_number($ratings_number) {
        $this->ratings_number = $ratings_number;
    }

    function setRating($rating) {
        $this->rating = $rating;
    }
    
    function setProduct($product) {
        $this->product = $product;
    }

    function setGallery($gallery) {
        $this->gallery = $gallery;
    }

    function setRegion($region) {
        $this->region = $region;
    }

}
