<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="product_categories")
 */
class ProductCategory {

    /**
     * @Id
     * @Column(type="integer", nullable=false, options={"unsigned":true})
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Column(type="string", nullable=false, length=50)
     */
    protected $name;

    /**
     * @Column(type="integer", nullable=true, options={"unsigned":true})
     */
    protected $weight;

    /**
     * @OneToMany(targetEntity="Product", mappedBy="category", cascade={"remove"})     
     */
    protected $product;

    public function __construct() {
        
    }

    function getId() {
        return $this->id;
    }

    function getCreate_date() {
        return $this->create_date;
    }

    function getName() {
        return $this->name;
    }

    function getWeight() {
        return $this->weight;
    }

    function getProduct() {
        return $this->product;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setCreate_date($create_date) {
        $this->create_date = $create_date;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setWeight($weight) {
        $this->weight = $weight;
    }

    function setProduct($product) {
        $this->product = $product;
    }

}
