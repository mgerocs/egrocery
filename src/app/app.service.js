(function () {
    'use strict';

    var GroceryApp = angular.module('GroceryApp');

    //*** USER SERVICE ***//
    GroceryApp.service('User', User);
    User.$inject = ['$http', 'BaseUrl'];
    function User($http, BaseUrl) {
        var service = this;
        var _isLoggedIn = false;
        var _email = "";
        service.checkIfLoggedIn = function () {
            return _isLoggedIn;
        };
        service.setIfLoggedIn = function (isLoggedIn) {
            _isLoggedIn = isLoggedIn;
        };
        service.getEmail = function () {
            return _email;
        };
        service.setEmail = function (email) {
            _email = email;
        };
        service.login = function (email, password) {
            var response = $http({
                method: "POST",
                url: (BaseUrl + "identity/checkCredentials"),
                data: {
                    'email': email,
                    'password': password
                },
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });
            return response;
        };
        service.logout = function () {
            var response = $http({
                method: "GET",
                url: (BaseUrl + "identity/logout")
            });
            return response;
        };
        service.getUser = function () {
            return {
                is_logged_in: _isLoggedIn,
                email: _email
            };
        };
        service.setUser = function (userdata) {
            _isLoggedIn = userdata.is_logged_in;
            _email = (userdata.email || "");
        };
    }
    ;

    //*** PRODUCT CATEGORY SERVICE ***//
    GroceryApp.service('ProductCategory', ProductCategory);
    ProductCategory.$inject = ['$sessionStorage'];
    function ProductCategory($sessionStorage) {
        var service = this;
        var _categories = [];
        service.getCategoryName = function (category_id) {
            return _categories[category_id].name;
        };
        service.getCategories = function () {
            return _categories;
        };
        service.setCategories = function (categories) {
            _categories = categories;
            $sessionStorage.categories = this.getCategories();
        };
        return service;
    }
    ;

    //*** PRODUCT SERVICE ***//
    GroceryApp.service('Product', Product);
    Product.$inject = ['$http', 'BaseUrl'];
    function Product($http, BaseUrl) {
        var service = this;
        service.getProductLabels = function (category_id) {
            var response = $http({
                method: "GET",
                url: (BaseUrl + "grocery/getProductLabels/" + category_id)
            });
            return response;
        };
        service.getProducts = function (filter) {
            var response = $http({
                method: "POST",
                url: (BaseUrl + "grocery/getProducts"),
                data: {'filter': filter},
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });
            return response;
        };
    }
    ;

    //*** REGION SERVICE ***//
    GroceryApp.service('Region', Region);
    function Region() {
        var service = this;
        var _regions = [];
        service.getRegions = function () {
            return _regions;
        };
        service.setRegions = function (regions) {
            _regions = regions;
        };
        return service;
    }
    ;
    
    //*** FARM SERVICE ***//
    GroceryApp.service('Farm', Farm);
    Farm.$inject = ['$http', 'BaseUrl'];
    function Farm($http, BaseUrl) {
        var service = this;
        service.getFarms = function () {
            var response = $http({
                method: "GET",
                url: (BaseUrl + "grocery/getFarms")
            });
            return response;
        };
        service.getFarm = function (farm_id) {
            var response = $http({
                method: "GET",
                url: (BaseUrl + "grocery/getFarms/" + farm_id)
            });
            return response;
        };
        service.showOnMap = function (lat, lon) {
            var target = new google.maps.LatLng(lat, lon);
            //var center = new google.maps.LatLng(47.162494, 19.503304);
            var center = new google.maps.LatLng(lat, lon);
            var marker;
            var map;

            var mapOptions = {
                zoom: 8,
                center: center,
                disableDefaultUI: true,
                scrollwheel: false,
            };

            map = new google.maps.Map(document.getElementById('map'), mapOptions);

            marker = new google.maps.Marker({
                map: map,
                draggable: false,                
                animation: google.maps.Animation.DROP,
                position: target
            });
        };
    }
    ;
    
    //*** PRODUCT FILTER SERVICE ***//
    GroceryApp.service('ProductFilter', ProductFilter);
    ProductFilter.$inject = ['$sessionStorage'];
    function ProductFilter($sessionStorage) {
        var service = this;
        var _filter = {};
        service.getFilter = function () {
            return _filter;
        };
        service.setFilterCategory = function (category_id) {
            _filter.category_id = category_id;
            $sessionStorage.filter = this.getFilter();
        };
        service.setFilterRegion = function (region_id) {
            _filter.region_id = region_id;
            $sessionStorage.filter = this.getFilter();
        };
        service.setFilterFarm = function (farm_id) {
            _filter.farm_id = farm_id;
            $sessionStorage.filter = this.getFilter();
        };
        service.setFilterLabel = function (label) {
            _filter.label = label;
            $sessionStorage.filter = this.getFilter();
        };
        service.setFilterPId = function (product_id) {
            _filter.product_id = product_id;
            $sessionStorage.filter = this.getFilter();
        };
        service.setFilter = function (filter) {
            _filter = filter;
            $sessionStorage.filter = this.getFilter();
        };
        service.reset = function () {
            // _filter.category_id = null;
            _filter.region_id = null;
            _filter.farm_id = null;
            _filter.label = null;
            _filter.product_id = null;
            $sessionStorage.filter = this.getFilter();
        };
        return service;
    }
    ;

    //*** CART SERVICE ***//
    GroceryApp.service('Cart', Cart);
    Cart.$inject = ['$sessionStorage'];
    function Cart($sessionStorage) {
        var _cart_id = null;
        var _cart_date;
        var _items = {};
        var _pcs;
        var _total;
        this.initCart = function () {
            _cart_id = this.generateId(8);
            _cart_date = new Date();
            _items = {};
            _pcs = 0;
            _total = 0;
        };
        this.getCartId = function () {
            return _cart_id;
        };
        this.addToCart = function (item) {
            var product = item.product;
            var amount = item.amount;
            if (!this.checkIfInCart(product)) {
                var newitem = {
                    'id': product.id,
                    'category_id': product.category_id,
                    'label': product.label,
                    'name': product.name,
                    'farm_id': product.farm_id,
                    'region_id': product.region_id,
                    'farmname': product.farmname,
                    'price': product.price,
                    'cover': product.cover,
                    'amount': amount                    
                };
                _items[product.id] = newitem;
            } else {
                _items[product.id].amount += amount;
            }

            this.calculatePrice();
            $sessionStorage.cart = this.getCart();
            return {
                'products': _items,
                'total': _total
            };
        };
        this.calculatePrice = function () {
            var sum = 0;
            var pcs = 0;
            for (var key in _items) {
                sum += _items[key].amount * _items[key].price;
                pcs += 1;
            }
            _total = sum;
            _pcs = pcs;
        };
        this.generateId = function (len) {
            var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
            var string_length = len;
            var randomstring = '';
            for (var x = 0; x < string_length; x++) {

                var letterOrNumber = Math.floor(Math.random() * 2);
                if (letterOrNumber === 0) {
                    var newNum = Math.floor(Math.random() * 9);
                    randomstring += newNum;
                } else {
                    var rnum = Math.floor(Math.random() * chars.length);
                    randomstring += chars.substring(rnum, rnum + 1);
                }
            }
            return randomstring;
        };
        this.checkIfInCart = function (product) {
            return product.id in _items;
        };
        this.getCart = function () {
            return {
                'cart_id': _cart_id,
                'cart_date': _cart_date,
                'products': _items,
                'pcs': _pcs,
                'total': _total
            };
        };
        this.setCart = function (cart) {
            _cart_id = cart.id;
            _cart_date = cart.cart_date;
            for (var p in cart.products) {
                _items[p] = cart.products[p];
            }
            _pcs = cart.pcs;
            _total = cart.total;
        };
        this.emptyCart = function () {
            _items = {};
            _total = 0;
            $sessionStorage.cart = this.getCart();
        };
        this.modifyCart = function (id, amount) {
            _items[id].amount += amount;
            this.calculatePrice();
            $sessionStorage.cart = this.getCart();

        };
        this.removeFromCart = function (id) {
            delete _items[id];
            this.calculatePrice();
            $sessionStorage.cart = this.getCart();
        };

    }
    ;

    //*** NAVIGATION SERVICE ***//
    GroceryApp.service('Navigation', Navigation);
    function Navigation() {
        var service = this;
        var _currentMenuItem = "";
        service.getCurrentMenuItem = function () {
            return _currentMenuItem;
        };
        service.setCurrentMenuItem = function (menuitem) {
            _currentMenuItem = menuitem;
        };
        return service;
    }
    ;

})();
